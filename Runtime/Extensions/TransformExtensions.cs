using UnityEngine;

namespace UMNP.GamedevKit.Extensions
{
    public static class TransformExtensions
    {
        public static void Attach(this Transform transform, in Transform destination)
        {
            transform.SetParent(destination, false);
            transform.ResetLocal();
        }

        public static void ResetLocal(this Transform transform)
        {
            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.identity;
            transform.localScale = Vector3.one;
        }
    }
}
