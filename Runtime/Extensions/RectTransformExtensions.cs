using UnityEngine;

namespace UMNP.GamedevKit.Extensions
{
    public static class RectTransformExtensions
    {
        /// <summary>
        /// Sets the RectTransform to safely fill the destination RectTrannsform.
        /// </summary>
        public static void FitToRect(this RectTransform rectTransform, bool isCameraPortrait, in RectTransform destination)
        {
            var ratio = rectTransform.sizeDelta.x / rectTransform.sizeDelta.y;

            // portrait.
            if (isCameraPortrait)
            {
                rectTransform.sizeDelta = new Vector2(ratio * destination.sizeDelta.y, destination.sizeDelta.y);
                return;
            }

            rectTransform.sizeDelta = new Vector2(destination.sizeDelta.x, destination.sizeDelta.x / ratio);
        }

        /// <summary>
        /// A more intuitive method to set the anchor.
        /// </summary>
        public static void SetAnchor(this RectTransform rectTransform, Anchor anchor)
        {
            switch (anchor)
            {
                case Anchor.TopLeft:
                    rectTransform.anchorMin = new Vector2(0.0f, 1.0f);
                    rectTransform.anchorMax = new Vector2(0.0f, 1.0f);
                    break;
                case Anchor.TopCenter:
                    rectTransform.anchorMin = new Vector2(0.5f, 1.0f);
                    rectTransform.anchorMax = new Vector2(0.5f, 1.0f);
                    break;
                case Anchor.TopRight:
                    rectTransform.anchorMin = Vector2.one;
                    rectTransform.anchorMax = Vector2.one;
                    break;
                case Anchor.MiddleLeft:
                    rectTransform.anchorMin = new Vector2(0.0f, 0.5f);
                    rectTransform.anchorMax = new Vector2(0.0f, 0.5f);
                    break;
                case Anchor.Center:
                    rectTransform.anchorMin = new Vector2(0.5f, 0.5f);
                    rectTransform.anchorMax = new Vector2(0.5f, 0.5f);
                    break;
                case Anchor.MiddleRight:
                    rectTransform.anchorMin = new Vector2(1.0f, 0.5f);
                    rectTransform.anchorMax = new Vector2(1.0f, 0.5f);
                    break;
                case Anchor.BottomLeft:
                    rectTransform.anchorMin = Vector2.zero;
                    rectTransform.anchorMax = Vector2.zero;
                    break;
                case Anchor.BottomCenter:
                    rectTransform.anchorMin = new Vector2(0.5f, 0.0f);
                    rectTransform.anchorMax = new Vector2(0.5f, 0.0f);
                    break;
                case Anchor.BottomRight:
                    rectTransform.anchorMin = new Vector2(1.0f, 0.0f);
                    rectTransform.anchorMax = new Vector2(1.0f, 0.0f);
                    break;
                case Anchor.TopStretch:
                    rectTransform.anchorMin = new Vector2(0.0f, 1.0f);
                    rectTransform.anchorMax = Vector2.one;
                    break;
                case Anchor.MiddleStretch:
                    rectTransform.anchorMin = new Vector2(0.0f, 0.5f);
                    rectTransform.anchorMax = new Vector2(1.0f, 0.5f);
                    break;
                case Anchor.BottomStretch:
                    rectTransform.anchorMin = Vector2.zero;
                    rectTransform.anchorMax = new Vector2(1.0f, 0.0f);
                    break;
                case Anchor.LeftStretch:
                    rectTransform.anchorMin = Vector2.zero;
                    rectTransform.anchorMax = new Vector2(0.0f, 1.0f);
                    break;
                case Anchor.CenterStretch:
                    rectTransform.anchorMin = new Vector2(0.5f, 0.0f);
                    rectTransform.anchorMax = new Vector2(0.5f, 1.0f);
                    break;
                case Anchor.RightStretch:
                    rectTransform.anchorMin = new Vector2(1.0f, 0.0f);
                    rectTransform.anchorMax = Vector2.one;
                    break;
                case Anchor.Stretch:
                    rectTransform.anchorMin = Vector2.zero;
                    rectTransform.anchorMax = Vector2.one;
                    rectTransform.sizeDelta = Vector2.zero;
                    break;
            }
        }

        public enum Anchor
        {
            TopLeft,
            TopCenter,
            TopRight,
            MiddleLeft,
            Center,
            MiddleRight,
            BottomLeft,
            BottomCenter,
            BottomRight,
            TopStretch,
            MiddleStretch,
            BottomStretch,
            LeftStretch,
            CenterStretch,
            RightStretch,
            Stretch
        }
    }
}
