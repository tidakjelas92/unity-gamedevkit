using UnityEngine;
using UnityEngine.UI;

namespace UMNP.GamedevKit.Extensions
{
    public static class ImageExtensions
    {
        /// <summary>
        /// Disable if null, set sprite and call SetNativeSize otherwise
        /// </summary>
        public static void SetSprite(this Image image, Sprite sprite)
        {
            image.gameObject.SetActive(sprite);
            if (!sprite) return;

            image.sprite = sprite;
            image.SetNativeSize();
        }
    }
}
