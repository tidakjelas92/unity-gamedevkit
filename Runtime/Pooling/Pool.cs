using System.Collections.Generic;
using UnityEngine;

namespace UMNP.GamedevKit.Pooling
{
    public class Pool
    {
        private Transform _transform;
        private Queue<GameObject> _queue = new Queue<GameObject>();

        public Transform Transform => _transform;
        public Queue<GameObject> Queue => _queue;

        public Pool(in Transform transform) => _transform = transform;
    }
}
