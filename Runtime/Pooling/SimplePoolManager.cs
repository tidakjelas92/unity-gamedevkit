using System;
using System.Collections.Generic;
using UnityEngine;

namespace UMNP.GamedevKit.Pooling
{
    /// <summary>
    /// Not really a pool manager, but provides a simple functionality of registering and clearing of MonoBehaviour objects.
    /// </summary>
    [Serializable]
    public abstract class SimplePoolManager<T> where T : MonoBehaviour
    {
        private List<T> _poolObjects = new List<T>();

        public void Add(T obj) => _poolObjects.Add(obj);

        public void Clean()
        {
            foreach (var obj in _poolObjects)
                MonoBehaviour.Destroy(obj.gameObject);

            _poolObjects.Clear();
        }

        public void Remove(T obj) => _poolObjects.Remove(obj);
    }
}
