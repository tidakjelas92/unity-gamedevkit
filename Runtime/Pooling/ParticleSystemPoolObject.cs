using UMNP.GamedevKit.Utils;
using UnityEngine;

namespace UMNP.GamedevKit.Pooling
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(ParticleSystem))]
    public abstract class ParticleSystemPoolObject : MonoBehaviour
    {
        // This must be serialized to the prefab file.
        [Tooltip("Pool id of this object. Make sure this object is registered to ParticleSystemManager and and press UpdateID button to generate the id.")]
        [SerializeField, ReadOnly] private int _id = -1;

        private ParticleSystem _particleSystem;

        internal int Id { get => _id; set => _id = value; }
        protected ParticleSystemPoolObject Instance => ParticleSystemManager.Instance.GetInstance(_id);

        private void Awake() => _particleSystem = GetComponent<ParticleSystem>();

        /// <summary>
        /// Clear the cache. Will need to be initialized next time Emit is called.
        /// </summary>
        public void Clear() =>
            ParticleSystemManager.Instance.Clear(_id);

        public abstract void Emit(in Vector3 position);

        /// <summary>
        /// (Optional) Use this to cache the system instead of lazy loading.
        /// </summary>
        public void Initialize() => ParticleSystemManager.Instance.Load(_id);
        public void Play() => Instance._particleSystem.Play();
        public void Play(in Vector3 position)
        {
            Instance.transform.position = position;
            Play();
        }
        public void Stop()
        {
            Instance._particleSystem.Stop();
            Instance._particleSystem.Clear(true);
        }

        protected void EmitParticle(ParticleSystem.EmitParams emitParams, int amount) => Instance._particleSystem.Emit(emitParams, amount);

        protected void EmitParticle(in Vector3 position, in Vector3 rotation = default, in Vector3 velocity = default, int amount = 1)
        {
            var emitParams = new ParticleSystem.EmitParams();

            emitParams.position = position;
            emitParams.velocity = velocity;
            emitParams.rotation3D = rotation;

            Instance._particleSystem.Emit(emitParams, amount);
        }
    }
}
