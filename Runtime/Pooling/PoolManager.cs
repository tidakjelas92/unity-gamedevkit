﻿/*
Code from Sebastian Lague YouTube channel
With some changes here and there.
*/


using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif // UNITY_EDITOR
using UnityEngine;
using UMNP.GamedevKit.Core;

namespace UMNP.GamedevKit.Pooling
{
    public class PoolManager : Singleton<PoolManager>
    {
        [SerializeField] private GameObject[] _poolItems = new GameObject[] { };

        private Dictionary<int, Pool> _pools = new Dictionary<int, Pool>();

        protected override void Awake()
        {
            base.Awake();

            for (var i = 0; i < _poolItems.Length; i++)
                CreatePool(i);
        }

        /// <remarks>
        /// Get prefabId from PoolObject.Id
        /// </remarks>
        public GameObject Spawn(int prefabId, Transform parent = null, bool worldPositionStays = true)
        {
#if UNITY_EDITOR
            if (!_pools.ContainsKey(prefabId))
            {
                Debug.LogError("Seems like the prefab you are trying to spawn hasn't been registered to the PoolItems, ", this);
                return null;
            }
#endif // UNITY_EDITOR

            var poolQueue = _pools[prefabId].Queue;

            GameObject instance;
            if (poolQueue.Count > 0)
            {
                instance = poolQueue.Dequeue();

                instance.transform.SetParent(parent, worldPositionStays);
                instance.SetActive(true);
            }
            else
            {
                instance = Instantiate(_poolItems[prefabId], parent);
#if UNITY_EDITOR
                var poolObject = instance.GetComponent<PoolObject>();
                if (!poolObject)
                    Debug.LogError("No PoolItem component found in prefab, please attach one derived from it.", this);
#endif // UNITY_EDITOR
            }

            return instance;
        }

        /// <remarks>
        /// This is not the same as destroying it, references to the returned instance must be cleaned up manually.
        /// </remarks>
        public void Return(int prefabId, GameObject instance)
        {
#if UNITY_EDITOR
            if (!_pools.ContainsKey(prefabId))
            {
                Debug.LogError($"Pool with given id {prefabId} not found.", this);
                return;
            }
#endif // UNITY_EDITOR
            var pool = _pools[prefabId];

            instance.SetActive(false);
            instance.transform.SetParent(pool.Transform, false);
            pool.Queue.Enqueue(instance);
        }

#if UNITY_EDITOR
        public void UpdateID()
        {
            if (_poolItems.Length == 0)
                Debug.Log("No pool items registered yet.", this);

            for (var i = 0; i < _poolItems.Length; i++)
            {
                var poolItem = _poolItems[i];
                if (poolItem == null)
                {
                    Debug.LogWarning($"Entry at index {i} is null, make sure every entry is assigned properly.", this);
                    continue;
                }

                var path = AssetDatabase.GetAssetPath(poolItem);
                var prefab = PrefabUtility.LoadPrefabContents(path);

                prefab.GetComponent<PoolObject>().Id = i;

                PrefabUtility.SaveAsPrefabAsset(prefab, path);
                PrefabUtility.UnloadPrefabContents(prefab);
            }
        }
#endif // UNITY_EDITOR

        private void CreatePool(int poolId)
        {
            var poolObject = new GameObject(_poolItems[poolId].name + "_instances");
            var poolTransform = poolObject.transform;
            poolTransform.SetParent(transform);

            var pool = new Pool(poolTransform);
            _pools.Add(poolId, pool);
        }
    }
}
