using System;
using UnityEngine;
using UnityEngine.Audio;

namespace UMNP.GamedevKit
{
    public sealed class AudioManager : Core.Singleton<AudioManager>
    {
        [SerializeField] private AudioMixer _audioMixer;
        [SerializeField] private ChannelConfig[] _channelConfigs = new ChannelConfig[] { };

        private Channel[] _channels = new Channel[] { };

        /// <remarks>
        /// Make sure to cache the returned value because this uses string comparison.
        /// </remarks>
        public Channel GetChannel(in string name)
        {
            foreach (var channel in _channels)
            {
                if (channel.Name == name)
                    return channel;
            }

#if UNITY_EDITOR
            Utils.Logging.LogError($"Channel {name} does not exist yet!", this);
#endif
            return null;
        }

        [ContextMenu("Initialize")]
        public void Initialize()
        {
            _channels = new Channel[_channelConfigs.Length];
            for (var i = 0; i < _channelConfigs.Length; i++)
            {
                var channelConfig = _channelConfigs[i];
                _channels[i] = new Channel(
                    this,
                    channelConfig.name,
                    channelConfig.sources,
                    channelConfig.volumeParameterKey
                );
            }
        }

#if UNITY_EDITOR
        [ContextMenu("PrintChannels")]
        public void PrintChannels()
        {
            Debug.Log(_channels);
            foreach (var channel in _channels)
            {
                Debug.Log($"Channel: {channel.Name}, volume: {channel.Volume}");
                foreach (var source in channel.Sources)
                    Debug.Log($"Source: {source}");
            }
        }
#endif

        [Serializable]
        private struct ChannelConfig
        {
            public string name;
            public AudioSource[] sources;
            public string volumeParameterKey;
        }

        public class Channel
        {
            private AudioManager _manager;
            private string _name;
            private AudioSource[] _sources;
            private string _volumeParameterKey;

            /// <remarks>
            /// It is not recommended to go above 1.0f (0 decibels) without a very specific reason.<br/>
            /// 0.0f is complete silence. 1.0f is normal volume.
            /// </remarks>
            public float Volume
            {
                get
                {
                    float value;
                    _manager._audioMixer.GetFloat(_volumeParameterKey, out value);
                    var normalized = Mathf.Max(0.0001f, Mathf.Pow(10.0f, value / 20.0f));
                    return normalized;
                }
                set
                {
                    var clampedVolume = Mathf.Max(0.0001f, value);
                    var finalVolume = Mathf.Log10(clampedVolume) * 20.0f;
                    _manager._audioMixer.SetFloat(_volumeParameterKey, finalVolume);
                }
            }
            public string Name => _name;
            public AudioSource[] Sources => _sources;

            public Channel(AudioManager manager, string name, AudioSource[] sources, string volumeParameterKey)
            {
                _manager = manager;
                _name = name;
                _sources = sources;
                _volumeParameterKey = volumeParameterKey;
            }

            public float GetCurrentTimestamp(int index) => _sources[index].time;
            public AudioClip GetCurrentClip(int index) => _sources[index].clip;
            public AudioSource GetSource(int index) => _sources[index];
            public void Pause(int index) => _sources[index].Pause();
            public void Play(int index)
            {
                var source = _sources[index];
#if UNITY_EDITOR
                if (!source.clip)
                    Utils.Logging.LogError("The clip is empty, but you are trying to play music.", _manager);
#endif
                if (source.isPlaying) return;
                source.Play();
            }
            public void PlayOneShot(in AudioClip clip, float volume = 1.0f) => _sources[0].PlayOneShot(clip, volume);
            public void PlayOneShot(in Structures.AudioContainer audio) => _sources[0].PlayOneShot(audio.Clip, audio.Volume);
            public void UnPause(int index)
            {
                var source = _sources[index];
                if (source.time == 0.0f)
                    source.Play();
                source.UnPause();
            }

            public void Set(int index, in AudioClip clip, float volume = 1.0f, float time = 0.0f)
            {
                var source = _sources[index];
                if (source.clip == clip) return;

                source.Stop();
                source.clip = clip;
                source.loop = true;
                source.time = time;
                source.volume = volume;
            }
            public void Set(int index, in Structures.AudioContainer audio, float time = 0.0f) => Set(index, audio.Clip, audio.Volume, time);
            public void Stop(int index) => _sources[index].Stop();

#if UNITY_EDITOR
            [ContextMenu("PrintVolume")]
            private void PrintVolume() => Utils.Logging.Log(Volume, _manager);
#endif
        }
    }
}

