using System.Collections.Generic;
using UnityEngine;

namespace UMNP.GamedevKit.UI
{
    public class SafeArea : MonoBehaviour
    {
        public static RectOffset Offset { get; set; } = new RectOffset();

        private static List<SafeArea> _activeAreas = new List<SafeArea>();
        private RectTransform _rectTransform;

        private void Awake() => _rectTransform = GetComponent<RectTransform>();
        private void OnEnable()
        {
            _activeAreas.Add(this);
            Fit();
        }

        private void OnDisable() => _activeAreas.Remove(this);

        public static void ClearOffsets()
        {
            ResetOffset();
            FitAll();
        }

        public void Fit()
        {
            var safeArea = Screen.safeArea;

            var minAnchor = safeArea.position;
            var maxAnchor = minAnchor + safeArea.size;

            minAnchor.x /= Screen.width;
            minAnchor.y /= Screen.height;
            maxAnchor.x /= Screen.width;
            maxAnchor.y /= Screen.height;

            _rectTransform.anchorMin = minAnchor;
            _rectTransform.anchorMax = maxAnchor;

            _rectTransform.anchoredPosition = new Vector2(Offset.left - Offset.right, Offset.bottom - Offset.top) / 2.0f;
            _rectTransform.sizeDelta = new Vector2(-(Offset.left + Offset.right), -(Offset.top + Offset.bottom));
        }

        public static void FitAll()
        {
            foreach (var safeArea in _activeAreas)
                safeArea.Fit();
        }

        private static void ResetOffset()
        {
            Offset.top = 0;
            Offset.bottom = 0;
            Offset.left = 0;
            Offset.right = 0;
        }
    }
}
