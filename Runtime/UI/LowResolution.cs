using UnityEngine;
using UnityEngine.UI;

namespace UMNP.GamedevKit.UI
{
    public class LowResolution : MonoBehaviour
    {
        [Header("Internal References")]
        [SerializeField] private RawImage _mainRawImage;
        [SerializeField] private RawImage _uiRawImage;
        [Header("Parameters")]
        [SerializeField, Range(0.1f, 1.0f)] private float _resolutionMultiplier;
        [SerializeField] private RenderTextureFormat _mainTextureFormat;
        [SerializeField] private RenderTextureFormat _uiTextureFormat;

        private RenderTexture _mainRenderTexture;
        private RenderTexture _uiRenderTexture;

        public void DisableLowResolution(Camera targetCamera, Camera uiCamera) => SetRenderTexture(targetCamera, uiCamera, null, null);

        public void EnableLowResolution(Camera targetCamera, Camera uiCamera)
        {
            var originalResolution = new Vector2Int(Screen.width, Screen.height);
            var mainResolution = new Vector2(Screen.width, Screen.height);
            mainResolution *= _resolutionMultiplier;

            _mainRenderTexture = new RenderTexture(Mathf.CeilToInt(mainResolution.x), Mathf.CeilToInt(mainResolution.y), 16, _mainTextureFormat);
            _uiRenderTexture = new RenderTexture(originalResolution.x, originalResolution.y, 16, _uiTextureFormat);
            SetRenderTexture(targetCamera, uiCamera, _mainRenderTexture, _uiRenderTexture);
        }

        private void SetRenderTexture(Camera mainCamera, Camera uiCamera, in RenderTexture mainRenderTexture, in RenderTexture uiRenderTexture)
        {
            mainCamera.targetTexture = mainRenderTexture;
            _mainRawImage.texture = mainRenderTexture;
            uiCamera.targetTexture = uiRenderTexture;
            _uiRawImage.texture = uiRenderTexture;
        }
    }
}
