using UnityEngine;

namespace UMNP.GamedevKit.UI
{
    /// <summary>
    /// Tracks a world position to be converted to rect transform space.
    /// </summary>
    public class WorldPositionTracker : MonoBehaviour
    {
        [SerializeField] private Transform _target;
        [SerializeField] private Camera _activeCamera;
        [SerializeField] private RectTransform _targetRectTransform;

        private Vector2 _trackedPosition;
        private Vector2 _uiOffset;

        public Vector2 TrackedPosition => _trackedPosition;

        private void Start() => CalculateOffset();
        private void Update() => CalculatePosition();

        public void SetParentRect(RectTransform rectTransform)
        {
            _targetRectTransform = rectTransform;
            CalculateOffset();
        }

        public void SetTarget(Transform transform) => _target = transform;
        public void SetActiveCamera(Camera camera) => _activeCamera = camera;

        /// <remarks>Call this whenever the screen resolution changes to recalculate the offset.</remarks>
        public void CalculateOffset() => _uiOffset = new Vector2(
            _targetRectTransform.sizeDelta.x * 0.5f,
            _targetRectTransform.sizeDelta.y * 0.5f
        );

        /// <summary>Manually calculate</summary>
        public void CalculatePosition()
        {
            if (!_target || !_activeCamera) return;
            _trackedPosition = ToRectPosition(_target.position);
        }

        private Vector2 ToRectPosition(in Vector3 worldPosition)
        {
            var viewportPosition = _activeCamera.WorldToViewportPoint(worldPosition);
            var proportionalPosition = new Vector2(
                viewportPosition.x * _targetRectTransform.sizeDelta.x,
                viewportPosition.y * _targetRectTransform.sizeDelta.y
            );

            return proportionalPosition - _uiOffset;
        }
    }
}
