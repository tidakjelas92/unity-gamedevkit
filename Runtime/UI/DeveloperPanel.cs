using System;
using System.Collections.Generic;
using TMPro;
using UMNP.GamedevKit.Extensions;
using UnityEngine;
using UnityEngine.UI;

namespace UMNP.GamedevKit.UI
{
    public class DeveloperPanel : MonoBehaviour
    {
        [Header("References")]
        [SerializeField] private GameObject _panelObject;
        [SerializeField] private GameObject _openButtonObject;
        [SerializeField] private RectTransform _contentRect;

        private bool _doNotShowAgain;
        private Dictionary<string, GameObject> _entries = new Dictionary<string, GameObject>();

        public bool DoNotShowAgain { get => _doNotShowAgain; set => _doNotShowAgain = value; }

        private void Start() =>
            _panelObject.SetActive(false);

        public void AddLabel(in string id, string label) =>
            AddEntry(
                id,
                () => CreateLabel(label, _contentRect).gameObject
            );

        public void AddButtonLabel(in string id, string label, string buttonLabel, Action callback) =>
            AddEntry(
                id,
                () =>
                {
                    var layoutObject = new GameObject("HorizontalLayout");
                    var layout = layoutObject.AddComponent<HorizontalLayoutGroup>();
                    layout.childControlWidth = true;
                    layout.childControlHeight = true;
                    layout.childForceExpandWidth = false;
                    layout.childForceExpandHeight = false;
                    layout.spacing = 20.0f;
                    layout.childAlignment = TextAnchor.MiddleCenter;

                    var layoutTransform = layoutObject.transform;

                    CreateLabel(label, layoutTransform);

                    var buttonObject = new GameObject("Button", typeof(Image), typeof(Button), typeof(LayoutElement));
                    var buttonTransform = buttonObject.transform;
                    var buttonText = CreateLabel(buttonLabel, buttonTransform);
                    buttonText.color = new Color(0.3f, 0.3f, 0.3f);
                    buttonText.alignment = TextAlignmentOptions.Center;

                    var button = buttonObject.GetComponent<Button>();
                    button.onClick.AddListener(() => callback());

                    buttonTransform.SetParent(layoutTransform);
                    buttonTransform.localScale = Vector3.one;

                    var buttonLayoutElement = buttonObject.AddComponent<LayoutElement>();
                    buttonLayoutElement.preferredWidth = 250.0f;
                    buttonLayoutElement.preferredHeight = 100.0f;

                    var buttonTextRect = buttonText.GetComponent<RectTransform>();
                    buttonTextRect.SetAnchor(RectTransformExtensions.Anchor.Stretch);

                    layoutTransform.SetParent(_contentRect);
                    layoutTransform.localScale = Vector3.one;

                    return layoutObject;
                }
            );

        public void OnOpenButtonClicked()
        {
            _openButtonObject.SetActive(false);
            _panelObject.SetActive(true);
        }

        public void OnCloseButtonClicked()
        {
            if (_doNotShowAgain)
            {
                Destroy(gameObject);
                return;
            }

            _openButtonObject.SetActive(true);
            _panelObject.SetActive(false);
        }

        public void RemoveEntry(in string id)
        {
            if (!_entries.ContainsKey(id))
            {
                Debug.LogError("id is not found!", this);
                return;
            }

            Destroy(_entries[id].gameObject);
            _entries.Remove(id);
        }

        private void AddEntry(in string id, Func<GameObject> instantiationDelegate)
        {
            if (_entries.ContainsKey(id))
            {
                Destroy(_entries[id]);
                _entries[id] = null;
            }

            _entries[id] = instantiationDelegate();
        }

        private TextMeshProUGUI CreateLabel(in string label, in Transform parent = null)
        {
            var labelObject = new GameObject("Label");
            var labelText = labelObject.AddComponent<TextMeshProUGUI>();
            labelText.SetText(label);
            labelObject.transform.SetParent(parent);
            labelObject.transform.localScale = Vector3.one;

            return labelText;
        }
    }
}
