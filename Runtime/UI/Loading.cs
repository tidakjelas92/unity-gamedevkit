using UnityEngine;
using UnityEngine.UI;

namespace UMNP.GamedevKit.UI
{
    public class Loading : MonoBehaviour
    {
        [SerializeField] private Slider _slider;

        private RectTransform _rectTransform;
        private int _currentStep;
        private int _totalSteps;

        public int TotalSteps { get => _totalSteps; set => _totalSteps = value; }
        public int CurrentStep
        {
            get => _currentStep;
            set
            {
                _currentStep = Mathf.Clamp(value, 0, _totalSteps);
                _slider.value = (float)_currentStep / (float)_totalSteps;
            }
        }
        protected Slider Slider => _slider;

        protected virtual void Awake() => _rectTransform = GetComponent<RectTransform>();

        protected virtual void Start()
        {
            _slider.value = 0.0f;
            _currentStep = 0;
        }
    }
}
