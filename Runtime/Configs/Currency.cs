using UnityEngine;

namespace UMNP.GamedevKit.Configs
{
    [CreateAssetMenu(menuName = "GamedevKit/CurrencyConfig", fileName = "New CurrencyConfig")]
    public class Currency : ScriptableObject
    {
        [SerializeField] private string _name;
        [SerializeField] private Sprite _uiSprite;
        [SerializeField] private Sprite _smallUiSprite;
        [SerializeField] private Sprite _inGameSprite;
        [SerializeField] private Structures.IntRange _range;

        public string Name => _name;
        public Sprite UiSprite => _uiSprite;
        public Sprite InGameSprite => _inGameSprite;
        public Structures.IntRange Range => _range;
        public Sprite SmallUiSprite => _smallUiSprite;
    }
}
