using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace UMNP.GamedevKit.Utils
{
    public class JsonHandler
    {
        private JObject _data;

        public JsonHandler() { }
        public JsonHandler(in string json) => Parse(json);

        public bool GetBool(in string key, bool defaultValue = false)
        {
            var token = _data[key];
            if (token == null)
                return defaultValue;

            bool value;
            if (!bool.TryParse((string)token, out value))
                return defaultValue;

            return value;
        }

        public DateTime GetDateTime(in string key, DateTime defaultValue = default)
        {
            var token = _data[key];
            if (token == null)
                return defaultValue;

            DateTime value;
            if (!DateTime.TryParse((string)token, out value))
                return defaultValue;

            return value;
        }

        public float GetFloat(in string key, float defaultValue = 0.0f)
        {
            var token = _data[key];
            if (token == null)
                return defaultValue;

            float value;
            if (!float.TryParse((string)token, out value))
                return defaultValue;

            return value;
        }

        public Guid GetGuid(in string key, Guid defaultValue = default)
        {
            var token = _data[key];
            if (token == null)
                return defaultValue;

            Guid value;
            if (!Guid.TryParse((string)token, out value))
                return defaultValue;
            return value;
        }

        public Guid[] GetGuids(in string key, Guid[] defaultValue = default)
        {
            var token = _data[key];

            if (token == null)
                return defaultValue;

            var array = JArray.Parse(token.ToString());
            var value = new List<Guid>();
            foreach (var child in array.Children())
            {
                Guid result;
                if (Guid.TryParse(child.ToString(), out result))
                    value.Add(result);
            }
            return value.ToArray();
        }

        public int GetInt(in string key, int defaultValue = 0)
        {
            var token = _data[key];
            if (token == null)
                return defaultValue;

            int value;
            if (!int.TryParse((string)token, out value))
                return defaultValue;

            return value;
        }

        public long GetLong(in string key, long defaultValue = 0)
        {
            var token = _data[key];
            if (token == null)
                return defaultValue;

            long value;
            if (!long.TryParse((string)token, out value))
                return defaultValue;

            return value;
        }

        public Dictionary<string, object> GetDictionary(in string key, Dictionary<string, object> defaultValue = default)
        {
            var token = _data[key];
            if (token == null)
                return defaultValue;

            var value = JsonConvert.DeserializeObject<Dictionary<string, object>>(token.ToString());
            return value;
        }

        public string GetString(in string key, string defaultValue = "")
        {
            var token = _data[key];

            if (token == null)
                return defaultValue;

            return token.ToString();
        }

        public TimeSpan GetTimeSpan(in string key, TimeSpan defaultValue = default)
        {
            var token = _data[key];

            if (token == null)
                return defaultValue;

            TimeSpan value;
            if (!TimeSpan.TryParse((string)token, out value))
                return defaultValue;

            return value;
        }


        public void Parse(in string json)
        {
            if (json == string.Empty)
            {
                AssignEmptyObject();
                return;
            }

            try
            {
                _data = JObject.Parse(json);
            }
            catch (JsonReaderException exception)
            {
                Logging.LogError($"Error parsing the json. Exception: {exception}");
                AssignEmptyObject();
            }

            JObject AssignEmptyObject() => _data = new JObject();
        }

        public static string ToJson(in Dictionary<string, object> data) => JsonConvert.SerializeObject(data);
    }
}
