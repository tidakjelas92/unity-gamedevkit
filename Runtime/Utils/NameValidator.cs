using System.Text.RegularExpressions;
using UMNP.GamedevKit.Structures;

namespace UMNP.GamedevKit.Utils
{
    // Currently only for Windows
    // WIP: convert this to service provider pattern

    /// <summary>
    /// Use this to validate a name before operating any IO on them.
    /// </summary>
    public static class NameValidator
    {
        private const string EmptyNameMessage = "Name cannot be empty string.";
        private const string IllegalNameMessage = "Name is illegal.";
        private const string IllegalCharacterMessage = "Name contains illegal character.";
        private const string ExtensionIllegalCharacterMessage = "Extension contains illegal character.";
        private const string ExtensionEndsWithDotMessage = "Extension cannot end with '.'";
        private const string ExtensionEndsWithWhitespaceMessage = "Extension cannot end with whitespace.";
        private const string EndsWithDotMessage = "Name cannot end with '.'";
        private const string EndsWithWhitespaceMessage = "Name cannot end with whitespace.";

        public static Status CheckFolderNameWindows(in string name)
        {
            var status = new Status();

            if (string.IsNullOrEmpty(name))
            {
                status.message = EmptyNameMessage;
                return status;
            }

            if (IsIllegalNameWindows(name))
            {
                status.message = IllegalNameMessage;
                return status;
            }

            if (ContainsIllegalCharacterWindows(name))
            {
                status.message = IllegalCharacterMessage;
                return status;
            }

            if (name.EndsWith("."))
            {
                status.message = EndsWithDotMessage;
                return status;
            }

            if (name.EndsWith(" "))
            {
                status.message = EndsWithWhitespaceMessage;
                return status;
            }

            status.success = true;
            return status;
        }

        public static Status CheckFileNameWindows(in string name, in string ext = "")
        {
            var status = new Status();

            if (string.IsNullOrEmpty(name))
            {
                status.message = EmptyNameMessage;
                return status;
            }

            if (IsIllegalNameWindows(name))
            {
                status.message = IllegalNameMessage;
                return status;
            }

            if (ContainsIllegalCharacterWindows(name))
            {
                status.message = IllegalCharacterMessage;
                return status;
            }

            if (ContainsIllegalCharacterWindows(ext))
            {
                status.message = ExtensionIllegalCharacterMessage;
                return status;
            }

            if (name.EndsWith("."))
            {
                status.message = EndsWithDotMessage;
                return status;
            }

            if (ext.EndsWith("."))
            {
                status.message = ExtensionEndsWithDotMessage;
                return status;
            }

            if (name.EndsWith(" "))
            {
                status.message = EndsWithWhitespaceMessage;
                return status;
            }

            if (ext.EndsWith(" "))
            {
                status.message = ExtensionEndsWithWhitespaceMessage;
                return status;
            }

            status.success = true;
            return status;
        }

        private static bool IsIllegalNameWindows(in string name)
        {
            /*
            Illegal names in windows:
            CON, PRN, NUL, AUX
            ., ..
            COM[0-9]
            LPT[0-9]
            */

            var forbidden = @"(CON)|(PRN)|(NUL)|(AUX)|(\.{1,2})|(LPT[0-9])|(COM[0-9])";
            var forbiddenRegex = new Regex(forbidden, RegexOptions.IgnoreCase);
            return forbiddenRegex.Matches(name).Count > 0;
        }

        private static bool ContainsIllegalCharacterWindows(in string name)
        {
            /*
            Forbidden characters in windows:
            \, /, :, ", |, <, >, *, ?
            */

            var forbiddenChars = new string[] { "\\", "/", ":", "\"", "|", "<", ">", "*", "?" };
            for (var i = 0; i < forbiddenChars.Length; i++)
            {
                if (name.Contains(forbiddenChars[i]))
                    return true;
            }

            return false;
        }
    }
}
