using UnityEngine;

namespace UMNP.GamedevKit.Utils
{
    /// <summary>
    /// Collection of conversion constants.
    /// </summary>
    public static class Units
    {
        /// <summary>Meters per second to Kilometers per hour.</summary>
        public const float MpsToKph = 3.6f;

        /// <summary>Meters per second to Miles per hour.</summary>
        public const float MpsToMph = 2.23694f;

        /// <summary>Kilometers per Hour to Miles per hour.</summary>
        public const float KphToMph = 0.621371f;

        /// <summary>Miles per hour to Kilometers per hour.</summary>
        public const float MphToKph = 1.60934f;

        /// <summary>Density-independent Pixel (dp or dip) => actual pixels on the screen.</summary>
        public static float ToPixel(float dp) => dp * (Screen.dpi / 160.0f);

        /// <summary>Actual pixels on the screen => Density-independent Pixel (dp or dip).</summary>
        public static float ToDP(float pixel) => pixel / (Screen.dpi / 160.0f);
    }
}
