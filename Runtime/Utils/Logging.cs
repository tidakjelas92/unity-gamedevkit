using UnityEngine;

namespace UMNP.GamedevKit.Utils
{
    /// <summary>
    /// Wrapper for logging
    /// </summary>
    public static class Logging
    {
        private static Configs.App _appConfig;

        private static bool IsInitialized => _appConfig != null;

        public static void Initialize(Configs.App appConfig) =>
            _appConfig = appConfig;

        public static void Log(object message)
        {
            if (!_appConfig) return;
            if (_appConfig.Build == Configs.App.BuildType.Release) return;

            Debug.Log(message);
        }

        public static void Log(object message, Object context)
        {
            if (!_appConfig) return;
            if (_appConfig.Build == Configs.App.BuildType.Release) return;

            Debug.Log(message, context);
        }

        public static void LogAssertion(object message)
        {
            if (!_appConfig) return;
            if (_appConfig.Build == Configs.App.BuildType.Release) return;

            Debug.LogAssertion(message);
        }

        public static void LogAssertion(object message, Object context)
        {
            if (!_appConfig) return;
            if (_appConfig.Build == Configs.App.BuildType.Release) return;

            Debug.LogAssertion(message, context);
        }

        public static void LogError(object message) => Debug.LogError(message);
        public static void LogError(object message, Object context) => Debug.LogError(message, context);
        public static void LogException(System.Exception exception) => Debug.LogException(exception);
        public static void LogException(System.Exception exception, Object context) => Debug.LogException(exception, context);

        public static void LogTrace(object message)
        {
            if (!_appConfig) return;
            if (_appConfig.Build != Configs.App.BuildType.Debug) return;

            Debug.Log(message);
        }

        public static void LogTrace(object message, Object context)
        {
            if (!_appConfig) return;
            if (_appConfig.Build != Configs.App.BuildType.Debug) return;

            Debug.Log(message, context);
        }

        public static void LogWarning(object message)
        {
            if (!_appConfig) return;
            if (_appConfig.Build == Configs.App.BuildType.Release) return;

            Debug.LogWarning(message);
        }

        public static void LogWarning(object message, Object context = null)
        {
            if (!_appConfig) return;
            if (_appConfig.Build == Configs.App.BuildType.Release) return;

            Debug.LogWarning(message, context);
        }
    }
}
