namespace UMNP.GamedevKit.Utils
{
    public class RandomPicker<T>
    {
        private T[] _array;
        private int[] _intervals;
        private int _totalWeight;

        public RandomPicker(T[] array, int[] weights)
        {
            _array = array;
            UpdateWeights(weights);
        }

        public T Pick()
        {
            var randomWeight = UnityEngine.Random.Range(0, _totalWeight + 1);
            var selectedIndex = 0;

            for (var i = 0; i < _intervals.Length; i++)
            {
                if (_intervals[i] >= randomWeight)
                {
                    selectedIndex = i;
                    break;
                }
            }

            return _array[selectedIndex];
        }

        public void UpdateWeights(in int[] weights)
        {
#if UNITY_EDITOR
            if (_array.Length != weights.Length)
            {
                Logging.LogError($"Array length ({_array.Length}) does not match weights length ({weights.Length})");
                return;
            }
#endif

            _totalWeight = 0;
            _intervals = new int[weights.Length];

            for (var i = 0; i < weights.Length; i++)
            {
                _intervals[i] = _totalWeight + weights[i];
                _totalWeight += weights[i];
            }
        }
    }
}