using UnityEngine;

namespace UMNP.GamedevKit.Utils
{
    [RequireComponent(typeof(RectTransform))]
    public class PositionOverride : MonoBehaviour
    {
        [SerializeField] private Vector2 _overridePosition;

        private RectTransform _rectTransform;
        private Vector2 _originalPosition;

        private void Awake()
        {
            _rectTransform = GetComponent<RectTransform>();
            _originalPosition = _rectTransform.anchoredPosition;
        }

        public void Override() =>
            _rectTransform.anchoredPosition = _overridePosition;

        public void ResetOverride() =>
            _rectTransform.anchoredPosition = _originalPosition;
    }
}
