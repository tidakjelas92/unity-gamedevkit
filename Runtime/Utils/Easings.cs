// Code taken from easings.net, adapted to C#
// Credits to Andrey Sitnik and Ivan Solovev

using UnityEngine;

namespace UMNP.GamedevKit.Utils
{
    /// <summary>
    /// Collection of ease functions.
    /// </summary>
    /// <remarks>
    /// All parameters and return values are normalized to [0.0f, 1.0f]
    /// </remarks>
    public static class Easings
    {
        private const float C1 = 1.70158f;
        private const float C2 = C1 * 1.525f;
        private const float C3 = C1 + 1.0f;
        private const float C4 = (2.0f * Mathf.PI) / 3.0f;
        private const float C5 = (2.0f * Mathf.PI) / 4.5f;

        public static float InBack(float x) => C3 * x * x * x - C1 * x * x;
        public static float InBounce(float x) => 1.0f - OutBounce(1.0f - x);
        public static float InCirc(float x) => 1.0f - Mathf.Sqrt(1.0f - Mathf.Pow(x, 2.0f));
        public static float InCubic(float x) => x * x * x;
        public static float InElastic(float x) => -Mathf.Pow(2.0f, 10.0f * x - 10.0f) * Mathf.Sin((x * 10.0f - 10.75f) * C4);
        public static float InExpo(float x) => Mathf.Pow(2.0f, 10.0f * x - 10.0f);
        public static float InQuad(float x) => x * x;
        public static float InQuart(float x) => x * x * x * x;
        public static float InQuint(float x) => x * x * x * x * x;
        public static float InSine(float x) => 1.0f - Mathf.Cos(x * Mathf.PI / 2.0f);

        public static float InOutBack(float x) =>
            x < 0.5f ?
                (Mathf.Pow(2.0f * x, 2.0f) * ((C2 + 1.0f) * 2.0f * x - C2)) / 2.0f :
                (Mathf.Pow(2.0f * x - 2.0f, 2.0f) * ((C2 + 1.0f) * (x * 2.0f - 2.0f) + C2) + 2.0f) / 2.0f;

        public static float InOutBounce(float x) =>
            x < 0.5f ?
                (1.0f - OutBounce(1.0f - 2.0f * x)) / 2.0f :
                (1.0f + OutBounce(2.0f * x - 1.0f)) / 2.0f;

        public static float InOutCirc(float x) =>
            x < 0.5f ?
                (1.0f - Mathf.Sqrt(1.0f - Mathf.Pow(2.0f * x, 2.0f))) / 2.0f :
                (Mathf.Sqrt(1.0f - Mathf.Pow(-2.0f * x + 2.0f, 2.0f)) + 1.0f) / 2.0f;

        public static float InOutCubic(float x) =>
            x < 0.5f ?
                4.0f * x * x * x :
                1.0f - Mathf.Pow(-2.0f * x + 2.0f, 3.0f) / 2.0f;

        public static float InOutElastic(float x) =>
            x < 0.5f ?
                -(Mathf.Pow(2.0f, 20.0f * x - 10.0f) * Mathf.Sin((20.0f * x - 11.125f) * C5)) / 2.0f :
                (Mathf.Pow(2.0f, -20.0f * x + 10.0f) * Mathf.Sin((20.0f * x - 11.125f) * C5)) / 2.0f + 1.0f;

        public static float InOutExpo(float x) =>
            x < 0.5 ?
                Mathf.Pow(2.0f, 20.0f * x - 10.0f) / 2.0f :
                (2.0f - Mathf.Pow(2.0f, -20.0f * x + 10.0f)) / 2.0f;

        public static float InOutQuad(float x) =>
            x < 0.5f ?
                2.0f * x * x :
                1.0f - Mathf.Pow(-2.0f * x + 2.0f, 2.0f) / 2.0f;

        public static float InOutQuart(float x) =>
            x < 0.5f ?
                8.0f * x * x * x * x :
                1.0f - Mathf.Pow(-2.0f * x + 2.0f, 4.0f) / 2.0f;

        public static float InOutQuint(float x) =>
            x < 0.5f ?
                16.0f * x * x * x * x * x :
                1.0f - Mathf.Pow(-2.0f * x + 2.0f, 5.0f) / 2.0f;

        public static float InOutSine(float x) => -(Mathf.Cos(Mathf.PI * x) - 1.0f) / 2.0f;
        public static float OutBack(float x) => 1.0f + C3 * Mathf.Pow(x - 1.0f, 3.0f) + C1 * Mathf.Pow(x - 1.0f, 2.0f);

        public static float OutBounce(float x)
        {
            const float N1 = 7.5625f;
            const float D1 = 2.75f;

            if (x < 1.0f / D1)
                return N1 * x * x;

            if (x < 2.0f / D1)
            {
                x -= 1.5f / D1;
                return N1 * x * x + 0.75f;
            }

            if (x < 2.5f / D1)
            {
                x -= 2.25f / D1;
                return N1 * x * x + 0.9375f;
            }

            x -= 2.625f / D1;
            return N1 * x * x + 0.984375f;
        }

        public static float OutCirc(float x) => Mathf.Sqrt(1.0f - Mathf.Pow(x - 1.0f, 2.0f));
        public static float OutCubic(float x) => 1.0f - Mathf.Pow(1.0f - x, 3);
        public static float OutElastic(float x) => Mathf.Pow(2.0f, -10.0f * x) * Mathf.Sin((x * 10.0f - 0.75f) * C4) + 1.0f;
        public static float OutExpo(float x) => 1.0f - Mathf.Pow(2.0f, -10.0f * x);
        public static float OutQuad(float x) => 1.0f - (1.0f - x) * (1.0f - x);
        public static float OutQuart(float x) => 1.0f - Mathf.Pow(1.0f - x, 4.0f);
        public static float OutQuint(float x) => 1.0f - Mathf.Pow(1.0f - x, 5.0f);
        public static float OutSine(float x) => Mathf.Sin(x * Mathf.PI / 2.0f);

        /// <summary>
        /// Automatically choose ease function based on enum.
        /// </summary>
        public static float Ease(float x, Type ease)
        {
            var result = 0.0f;
            switch (ease)
            {
                case Type.InSine:
                    result = InSine(x);
                    break;
                case Type.OutSine:
                    result = OutSine(x);
                    break;
                case Type.InOutSine:
                    result = InOutSine(x);
                    break;
                case Type.InQuad:
                    result = InQuad(x);
                    break;
                case Type.OutQuad:
                    result = OutQuad(x);
                    break;
                case Type.InOutQuad:
                    result = InOutQuad(x);
                    break;
                case Type.InCubic:
                    result = InCubic(x);
                    break;
                case Type.OutCubic:
                    result = OutCubic(x);
                    break;
                case Type.InOutCubic:
                    result = InOutCubic(x);
                    break;
                case Type.InQuart:
                    result = InQuart(x);
                    break;
                case Type.OutQuart:
                    result = OutQuart(x);
                    break;
                case Type.InOutQuart:
                    result = InOutQuart(x);
                    break;
                case Type.InQuint:
                    result = InQuint(x);
                    break;
                case Type.OutQuint:
                    result = OutQuint(x);
                    break;
                case Type.InOutQuint:
                    result = InOutQuint(x);
                    break;
                case Type.InExpo:
                    result = InExpo(x);
                    break;
                case Type.OutExpo:
                    result = OutExpo(x);
                    break;
                case Type.InOutExpo:
                    result = InOutExpo(x);
                    break;
                case Type.InCirc:
                    result = InCirc(x);
                    break;
                case Type.OutCirc:
                    result = OutCirc(x);
                    break;
                case Type.InOutCirc:
                    result = InOutCirc(x);
                    break;
                case Type.InBack:
                    result = InBack(x);
                    break;
                case Type.OutBack:
                    result = OutBack(x);
                    break;
                case Type.InOutBack:
                    result = InOutBack(x);
                    break;
                case Type.InElastic:
                    result = InElastic(x);
                    break;
                case Type.OutElastic:
                    result = OutElastic(x);
                    break;
                case Type.InOutElastic:
                    result = InOutElastic(x);
                    break;
                case Type.InBounce:
                    result = InBounce(x);
                    break;
                case Type.OutBounce:
                    result = OutBounce(x);
                    break;
                case Type.InOutBounce:
                    result = InOutBounce(x);
                    break;
            }

            return result;
        }

        public enum Type
        {
            InSine, OutSine, InOutSine,
            InQuad, OutQuad, InOutQuad,
            InCubic, OutCubic, InOutCubic,
            InQuart, OutQuart, InOutQuart,
            InQuint, OutQuint, InOutQuint,
            InExpo, OutExpo, InOutExpo,
            InCirc, OutCirc, InOutCirc,
            InBack, OutBack, InOutBack,
            InElastic, OutElastic, InOutElastic,
            InBounce, OutBounce, InOutBounce
        }
    }
}
