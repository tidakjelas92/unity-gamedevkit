namespace UMNP.GamedevKit.Utils
{
    /// <summary>
    /// Sorts sprite renderer on OnEnable.
    /// </summary>
    public class StaticSpriteSorter : SpriteSorter
    {
        private void OnEnable() =>
            _spriteRenderer.sortingOrder = GetSpriteOrder();
    }
}
