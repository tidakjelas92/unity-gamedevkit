using UnityEngine;

namespace UMNP.GamedevKit.Utils
{
    [RequireComponent(typeof(SpriteRenderer)), DisallowMultipleComponent]
    public abstract class SpriteSorter : MonoBehaviour
    {
        [Tooltip("Lower value is better accuracy.")]
        [SerializeField] private float _accuracy = 1.0f;
        [SerializeField] private Structures.Axis _sortAxis = Structures.Axis.Y;
        [SerializeField] private int _offset;
        [SerializeField] private bool _invertSorting;

        protected SpriteRenderer _spriteRenderer;

        private Transform _transform;

        private void Awake()
        {
            _spriteRenderer = GetComponent<SpriteRenderer>();
            _transform = transform;
        }

        protected int GetSpriteOrder()
        {
            var invert = _invertSorting ? -1 : 1;
            switch (_sortAxis)
            {
                case Structures.Axis.X:
                    return (Mathf.FloorToInt(_transform.position.x / _accuracy) * invert) + _offset;
                case Structures.Axis.Y:
                    return (Mathf.FloorToInt(_transform.position.y / _accuracy) * invert) + _offset;
                default:
                    return (Mathf.FloorToInt(_transform.position.z / _accuracy) * invert) + _offset;
            }
        }
    }
}
