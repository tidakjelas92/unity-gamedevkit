using System;
using System.Collections;
using UnityEngine;

namespace UMNP.GamedevKit.Utils
{
    /// <summary>
    /// Collections of common coroutine setups.
    /// </summary>
    public static class Coroutines
    {
        public static IEnumerator DoAfter(Action action, int frames, Action everyLoopAction = null)
        {
            var elapsed = 0;
            while (elapsed < frames)
            {
                everyLoopAction?.Invoke();
                elapsed++;
                yield return null;
            }

            action?.Invoke();
        }

        public static IEnumerator DoAfter(Action action, float seconds, Action everyLoopAction = null)
        {
            var elapsed = 0.0f;
            while (elapsed < seconds)
            {
                everyLoopAction?.Invoke();
                elapsed += Time.deltaTime;
                yield return null;
            }

            action?.Invoke();
        }

        public static IEnumerator DoAfter(Action action, YieldInstruction yieldInstruction)
        {
            yield return yieldInstruction;

            action?.Invoke();
        }
    }
}
