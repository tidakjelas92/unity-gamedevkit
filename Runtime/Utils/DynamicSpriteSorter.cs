namespace UMNP.GamedevKit.Utils
{
    /// <summary>
    /// Sorts sprite renderer every frame.
    /// </summary>
    public class DynamicSpriteSorter : SpriteSorter
    {
        private void Update() =>
            _spriteRenderer.sortingOrder = GetSpriteOrder();
    }
}
