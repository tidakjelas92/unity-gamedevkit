using UnityEngine;

namespace UMNP.GamedevKit.Utils
{
    public class InfiniteRotator : MonoBehaviour
    {
        [SerializeField, Tooltip("Degree per second")] private Vector3 _rotationRate = new Vector3(0.0f, 30.0f, 0.0f);

        private Transform _transform;

        private void Awake() => _transform = transform;

        private void Update() =>
            _transform.Rotate(_rotationRate * Time.deltaTime);
    }
}
