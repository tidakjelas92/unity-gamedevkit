using UnityEngine;
using UnityEngine.UI;

namespace UMNP.GamedevKit.Utils
{
    [RequireComponent(typeof(Image))]
    public class ColorOverride : MonoBehaviour
    {
        [SerializeField] private Color _overrideColor = Color.white;

        private Image _image;
        private Color _originalColor;

        public Color OverrideColor { get => _overrideColor; set => _overrideColor = value; }

        private void Awake()
        {
            _image = GetComponent<Image>();
            _originalColor = _image.color;
        }

        public void Override() =>
            _image.color = _overrideColor;

        public void ResetOverride() =>
            _image.color = _originalColor;
    }
}
