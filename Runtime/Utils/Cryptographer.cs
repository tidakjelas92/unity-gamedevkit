using System;
using System.Security.Cryptography;
using System.Text;

namespace UMNP.GamedevKit.Utils
{
    /// <remarks>
    /// OFB and CTS cipher mode is not supported. Tested in Unity 2021.2.7f1
    /// </remarks>
    public class Cryptographer
    {
        private AesCryptoServiceProvider aes = new AesCryptoServiceProvider();

        public CipherMode Mode { get => aes.Mode; set => aes.Mode = value; }
        public PaddingMode Padding { get => aes.Padding; set => aes.Padding = value; }
        public string IV
        {
            get => Convert.ToBase64String(aes.IV);
            set => aes.IV = Convert.FromBase64String(value);
        }
        public string Key
        {
            get => Convert.ToBase64String(aes.Key);
            set => aes.Key = Convert.FromBase64String(value);
        }

        public void GenerateKey(CipherMode mode = CipherMode.CBC, PaddingMode padding = PaddingMode.PKCS7)
        {
            aes.Mode = mode;
            aes.Padding = padding;
            aes.GenerateIV();
            aes.GenerateKey();
        }

        public string Encrypt(in string data)
        {
            var dataBytes = Encoding.UTF8.GetBytes(data);
            var cryptoTransform = aes.CreateEncryptor(aes.Key, aes.IV);
            var result = cryptoTransform.TransformFinalBlock(dataBytes, 0, dataBytes.Length);

            return Convert.ToBase64String(result);
        }

        public string Decrypt(in string data)
        {
            var dataBytes = Convert.FromBase64String(data);
            var cryptoTransform = aes.CreateDecryptor(aes.Key, aes.IV);
            var result = cryptoTransform.TransformFinalBlock(dataBytes, 0, dataBytes.Length);

            return Encoding.UTF8.GetString(result);
        }
    }
}