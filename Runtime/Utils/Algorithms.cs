using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace UMNP.GamedevKit.Utils
{
    /// <summary>
    /// Collection of common algorithms.
    /// </summary>
    public static class Algorithms
    {
        public static bool Approximately(float a, float b, float marginOfError = float.Epsilon)
        {
            float difference = Mathf.Abs(a - b);
            return difference <= marginOfError;
        }

        public static bool ArrayContains<T>(in T[] array, in T value) where T : IEquatable<T>
        {
            foreach (var element in array)
            {
                if (element.Equals(value))
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Get the average point from given point array.
        /// </summary>
        public static Vector2 Average(in Vector2[] points)
        {
            var result = Vector2.zero;
            for (var i = 0; i < points.Length; i++)
                result += points[i];

            result /= points.Length;
            return result;
        }

        public static Vector2Int Clamp(in Vector2Int value, in Vector2Int min, in Vector2Int max)
        {
            var result = Vector2Int.zero;

            result.x = Mathf.Clamp(value.x, min.x, max.x);
            result.y = Mathf.Clamp(value.y, min.y, max.y);

            return result;
        }

        public static Vector3Int Clamp(in Vector3Int value, in Vector3Int min, in Vector3Int max)
        {
            var result = Vector3Int.zero;

            result.x = Mathf.Clamp(value.x, min.x, max.x);
            result.y = Mathf.Clamp(value.y, min.y, max.y);
            result.z = Mathf.Clamp(value.z, min.z, max.z);

            return result;
        }

        public static Vector2 FindLeftmostPoint(in Vector2[] points)
        {
            var result = points[0];
            if (points.Length > 1)
            {
                for (var i = 1; i < points.Length; i++)
                {
                    if (points[i].x < result.x)
                        result = points[i];
                }
            }

            return result;
        }

        public static Vector2 FindRightmostPoint(in Vector2[] points)
        {
            var result = points[0];
            if (points.Length > 1)
            {
                for (var i = 1; i < points.Length; i++)
                {
                    if (points[i].x > result.x)
                        result = points[i];
                }
            }

            return result;
        }

        public static Vector2 FindTopmostPoint(in Vector2[] points)
        {
            var result = points[0];
            if (points.Length > 1)
            {
                for (var i = 1; i < points.Length; i++)
                {
                    if (points[i].y > result.y)
                        result = points[i];
                }
            }

            return result;
        }

        public static Vector2 FindBottommostPoint(in Vector2[] points)
        {
            var result = points[0];
            if (points.Length > 1)
            {
                for (var i = 1; i < points.Length; i++)
                {
                    if (points[i].y < result.y)
                        result = points[i];
                }
            }

            return result;
        }

        /// <returns>
        /// Array with unique elements randomly chosen from the given array.
        /// </returns>
        /// <param name="count">How many elements should be returned.</param>
        public static T[] GenerateUniqueFromArray<T>(in T[] array, int count)
        {
#if UNITY_EDITOR
            if (IsNullOrEmpty(array))
            {
                Debug.LogError("Array cannot be null or empty!");
                return null;
            }

            if (count > array.Length)
            {
                Debug.LogError("Requested count can't be larger than the array itself.");
                return null;
            }

            if (array.Length == 1 || count == 1)
                Debug.LogWarning("It might be better not to use this method if the array size or the requested count is 1.");
#endif // UNITY_EDITOR

            var result = new T[count];
            var arrayList = new List<T>(array);

            for (int i = 0; i < count; i++)
            {
                var randomIndex = UnityEngine.Random.Range(0, arrayList.Count);
                var item = arrayList[randomIndex];
                result[i] = item;

                arrayList.Remove(item);
            }

            return result;
        }

        /// <returns>
        /// Array with unique integers randomly chosen in given range.
        /// </returns>
        /// <param name="count">How many integers should be returned.</param>
        public static int[] GenerateUniqueInRange(int min, int max, int count)
        {
            var range = max - min;
#if UNITY_EDITOR
            if (range <= 0)
            {
                Debug.LogError("Min and max value range should be greater than 0.");
                return null;
            }

            if (range == 1)
                Debug.LogWarning("It might be better not to use this method if the min and max is only 1 integer away from each other.");
#endif // UNITY_EDITOR

            var result = new int[count];
            var intList = new List<int>(Enumerable.Range(min, range));

            for (var i = 0; i < count; i++)
            {
                var randomIndex = UnityEngine.Random.Range(0, intList.Count);
                result[i] = randomIndex;
                intList.Remove(randomIndex);
            }

            return result;
        }

        /// <returns>
        /// Array of items selected from given weightedItems. Items can be duplicates.
        /// </returns>
        /// <param name="weightedItems">Weight can be any positive number.</param>
        public static T[] GenerateWeighted<T>((T, float)[] weightedItems, int count)
        {
#if UNITY_EDITOR
            if (IsNullOrEmpty(weightedItems))
            {
                Debug.LogError("Array cannot be null or empty!");
                return null;
            }

            if (count <= 0)
            {
                Debug.LogError("Requested count cannot be <= 0");
                return null;
            }
#endif // UNITY_EDITOR

            var intervals = new float[weightedItems.Length];
            var weight = 0.0f;
            for (var i = 0; i < weightedItems.Length; i++)
            {
#if UNITY_EDITOR
                if (weightedItems[i].Item2 <= 0.0f)
                    Debug.LogError($"Weight in index {i} is less than 0.");
#endif // UNITY_EDITOR
                weight += weightedItems[i].Item2;
                intervals[i] = weight;
            }

            var result = new T[count];
            for (var i = 0; i < count; i++)
            {
                var randomWeight = UnityEngine.Random.Range(0.0f, weight);
                for (var j = 0; j < intervals.Length; j++)
                {
                    if (randomWeight < intervals[j])
                    {
                        result[i] = weightedItems[j].Item1;
                        break;
                    }
                }
            }

            return result;
        }

        // Taken from https://dev.to/bogdanalexandru/generating-random-points-within-a-polygon-in-unity-nce
        public static Vector3 GenerateRandomPointInTriangle(in Vector3 vertex1, in Vector3 vertex2, in Vector3 vertex3)
        {
            var random1 = Mathf.Sqrt(UnityEngine.Random.Range(0.0f, 1.0f));
            var random2 = UnityEngine.Random.Range(0.0f, 1.0f);

            var m1 = 1.0f - random1;
            var m2 = random1 * (1.0f - random2);
            var m3 = random2 * random1;

            return m1 * vertex1 + m2 * vertex2 + m3 * vertex3;
        }

        public static int GetIndexOfLowestDistance(in Vector3 origin, in Vector3[] destinations)
        {
            var distances = new float[destinations.Length];
            for (var i = 0; i < destinations.Length; i++)
                distances[i] = Vector3.Distance(origin, destinations[i]);

            var selectedIndex = 0;
            var nearestDistance = distances[0];
            for (var i = 1; i < destinations.Length; i++)
            {
                if (distances[i] < nearestDistance)
                {
                    nearestDistance = distances[i];
                    selectedIndex = i;
                }
            }

            return selectedIndex;
        }

        public static T GetNearestObject<T>(in T origin, in IEnumerable<T> targets) where T : MonoBehaviour
        {
            var lowestDistance = float.MaxValue;
            T selectedTarget = null;
            foreach (var target in targets)
            {
                var distance = Vector3.Distance(origin.transform.position, target.transform.position);
                if (distance >= lowestDistance) continue;

                lowestDistance = distance;
                selectedTarget = target;
            }

            return selectedTarget as T;
        }

        /// <summary>
        /// Determine whether the given number needs 'st', 'nd', 'rd', or 'th'.
        /// </summary>
        public static string GetOrdinal(int number)
        {
            var lastDigit = number;
            if (number >= 10)
                lastDigit = number % 10;

            switch (lastDigit)
            {
                case 1:
                    return "st";
                case 2:
                    return "nd";
                case 3:
                    return "rd";
                default:
                    return "th";
            }
        }

        /// <summary>
        /// Converts the number to a formatted version of it. Example: 1234567 => 1,234,567
        /// </summary>
        public static string GetFormattedNumber(int number) => String.Format("{0:#,###0}", number);

        public static Vector2Int GetRandomCoord(in Vector2Int minInclusive, in Vector2Int maxExclusive) =>
            new Vector2Int(
                UnityEngine.Random.Range(minInclusive.x, maxExclusive.x),
                UnityEngine.Random.Range(minInclusive.y, maxExclusive.y)
            );

        public static string GetRoundedString(float value, int decimals) => decimal.Round((decimal)value, decimals).ToString();

        /// <remarks>
        /// Worst and average: O(n^2)<br/>
        /// Best case scenario: small data set, only a few misplaced elements.
        /// </remarks>
        public static void InsertionSort(ref int[] array)
        {
            for (var i = 1; i < array.Length; i++)
            {
                var value = array[i];
                var j = i;
                while (j > 0 && array[j - 1] > value)
                {
                    array[j] = array[j - 1];
                    j--;
                }
                array[j] = value;
            }
        }

        // was trying to emulate (min <= value < max), hence the placement of the positional arguments.
        public static bool IsInBetween(float minInclusive, float value, float maxExclusive) =>
            value >= minInclusive && value < maxExclusive;

        public static bool IsInBetween(int minInclusive, int value, int maxExclusive) =>
            value >= minInclusive && value < maxExclusive;

        public static bool IsNullOrEmpty(in Array array) => array == null || array.Length == 0;

        /// <remarks>
        /// Instead of using this, consider using List directly.
        /// </remarks>
        public static T[] PushBack<T>(in T[] array, T value)
        {
            var list = new List<T>(array);
            list.Add(value);
            return list.ToArray();
        }

        public static T[] RemoveDuplicatesIn<T>(in T[] array)
        {
            var hashSet = new HashSet<T>(array);

            var prunedSet = new T[hashSet.Count];
            hashSet.CopyTo(prunedSet);

            return prunedSet;
        }
    }
}
