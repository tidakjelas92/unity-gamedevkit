﻿using UnityEngine;


namespace UMNP.GamedevKit.Utils
{
    public class ReadOnlyAttribute : PropertyAttribute {}
}
