using System.IO;
using System.IO.Compression;

namespace UMNP.GamedevKit.Utils
{
    public static class Compressions
    {
        public static byte[] CompressDeflate(in string input, CompressionLevel compressionLevel = CompressionLevel.Optimal)
        {
            var inputBytes = Conversions.ToBytes(input);
            using (var outputStream = new MemoryStream())
            {
                using (var inputStream = new MemoryStream(inputBytes))
                {
                    using (var compressionStream = new DeflateStream(outputStream, compressionLevel))
                        inputStream.CopyTo(compressionStream);
                    return outputStream.ToArray();
                }
            }
        }

        public static string DecompressDeflate(in byte[] input)
        {
            using (var outputStream = new MemoryStream())
            {
                using (var inputStream = new MemoryStream(input))
                {
                    using (var decompressionStream = new DeflateStream(inputStream, CompressionMode.Decompress))
                        decompressionStream.CopyTo(outputStream);

                    return Conversions.ToString(outputStream.ToArray());
                }
            }
        }
    }
}
