using System;
using System.Collections.Generic;

namespace UMNP.GamedevKit.Utils
{
    public class ShopHandler
    {
        private LinkedList<Guid> _catalog;
        private LinkedListNode<Guid> _selection;
        private Guid _currentSelection;
        private ItemState _currentState;

        public Guid CurrentSelection
        {
            get => _currentSelection;
            set => _currentSelection = value;
        }
        public Guid Selection
        {
            get => _selection.Value;
            set
            {
                _selection = _catalog.Find(value);
                Refresh();
            }
        }
        public ItemState CurrentState
        {
            get => _currentState;
            private set
            {
                _currentState = value;
                OnItemStateChanged?.Invoke(_currentState);
            }
        }
        public bool IsNextPossible => _selection.Next != null;
        public bool IsPreviousPossible => _selection.Previous != null;

        public event Action<Guid, ItemState> OnSelectionChanged;
        public event Action<ItemState> OnItemStateChanged;

        private Func<Guid> _getCurrentSelected;
        private Func<Guid[]> _getUnlockedCatalog;
        private Predicate<Guid> _canItemBeBought;
        private Action<Guid> _onBuy;
        private Action<Guid> _onEquip;

        public ShopHandler(Guid[] catalog) => _catalog = new LinkedList<Guid>(catalog);

        public void Buy()
        {
            var selected = _selection.Value;
            _onBuy?.Invoke(selected);
            Refresh();
        }

        public void Equip()
        {
            _currentSelection = _selection.Value;
            _onEquip?.Invoke(_currentSelection);
            Refresh();
        }

        public void MoveNext()
        {
            _selection = _selection.Next;
            Refresh();
            OnSelectionChanged?.Invoke(_selection.Value, _currentState);
        }

        public void MovePrevious()
        {
            _selection = _selection.Previous;
            Refresh();
            OnSelectionChanged?.Invoke(_selection.Value, _currentState);
        }

        public void Refresh()
        {
            var selected = _selection.Value;
            var unlocked = _getUnlockedCatalog();
            var currentCharacter = _getCurrentSelected();

            if (selected == currentCharacter)
            {
                CurrentState = ItemState.Equipped;
                return;
            }

            if (Algorithms.ArrayContains<Guid>(unlocked, selected))
            {
                CurrentState = ItemState.Unlocked;
                return;
            }

            if (_canItemBeBought(selected))
            {
                CurrentState = ItemState.CanBuy;
                return;
            }

            CurrentState = ItemState.CannotBuy;
        }

        public void SetGetCurrentSelected(Func<Guid> callback) => _getCurrentSelected = callback;
        public void SetGetUnlockedCatalog(Func<Guid[]> callback) => _getUnlockedCatalog = callback;
        public void SetCanItemBeBought(Predicate<Guid> callback) => _canItemBeBought = callback;
        public void SetOnBuy(Action<Guid> callback) => _onBuy = callback;
        public void SetOnEquip(Action<Guid> callback) => _onEquip = callback;


        public enum ItemState
        {
            Equipped,
            Unlocked,
            CanBuy,
            CannotBuy
        }
    }
}
