using System;
using System.Threading;
using System.Threading.Tasks;

namespace UMNP.GamedevKit.Utils
{
    /// <example>
    /// How to use:
    /// 1. Instantiate the object.
    /// 2. Initialize.
    /// 3. When everything is ready, invoke Activate to start counting. This is done on another thread
    /// 4. Now you can query its values.
    /// 5. Don't forget to call CancelRestore, for example, when quitting the application.
    /// </example>
    public class EnergyHandler
    {
        private int _currentEnergy;
        private long _nextEnergyTime;
        private long _lastEnergyTime;
        private int _maxEnergy;
        private int _restoreDuration;
        private bool _isInitialized;
        private bool _isRestoring;
        private long _currentTime;
        private CancellationTokenSource _tokenSource;

        public int CurrentEnergy
        {
            get => _currentEnergy;
            private set
            {
                _currentEnergy = value;
                OnEnergyChanged?.Invoke(_currentEnergy);
            }
        }
        public int MaxEnergy { get => _maxEnergy; set => _maxEnergy = value; }
        /// <returns>Total seconds to next energy restore.</returns>
        public long NextRestore => _nextEnergyTime - CurrentTime;
        public int RestoreDuration { get => _restoreDuration; set => _restoreDuration = value; }
        private long CurrentTime => DateTimeOffset.Now.ToUnixTimeSeconds();

        public event Action<int> OnEnergyChanged;
        public Func<SaveData> LoadDelegate;
        public Action<SaveData> SaveDelegate;

        public void Activate()
        {
#if UNITY_EDITOR
            if (!_isInitialized)
                Utils.Logging.LogError("This EnergyHandler object is not initialized yet.");
#endif
            LoadSaveData();
            StartRestoring();
        }

        public void CancelRestore()
        {
            Logging.LogTrace("Cancel restore!");
            if (_tokenSource == null) return;

            _tokenSource.Cancel();
            _tokenSource.Dispose();
            _tokenSource = null;
        }

        /// <remarks>
        /// Check if CurrentEnergy >= energy before calling this method.
        /// </remarks>
        public void Consume(int energy)
        {
#if UNITY_EDITOR
            if (!_isInitialized)
                Utils.Logging.LogError("This EnergyHandler object is not initialized yet.");
#endif
            CurrentEnergy -= energy;
            SaveValues();

            if (_isRestoring) return;

            _nextEnergyTime = CurrentTime + _restoreDuration;
            StartRestoring();
        }

        public void Initialize(int maxEnergy, int restoreDuration, Func<SaveData> loadDelegate, Action<SaveData> saveDelegate)
        {
            if (_isInitialized) return;

            _maxEnergy = maxEnergy;
            _restoreDuration = restoreDuration;
            LoadDelegate = loadDelegate;
            SaveDelegate = saveDelegate;

            _isInitialized = true;
        }

        public void LoadSaveData()
        {
            var saveData = LoadDelegate();
            _currentEnergy = saveData.currentEnergy;
            _lastEnergyTime = saveData.lastEnergyTime;
            _nextEnergyTime = saveData.nextEnergyTime;
        }

        public void Restore(int value)
        {
            CurrentEnergy += value;
            if (_currentEnergy >= _maxEnergy)
                CancelRestore();

            SaveValues();
        }

        private async void RestoreAsync(CancellationToken token)
        {
#if UNITY_EDITOR
            if (!_isInitialized)
                Utils.Logging.LogError("This EnergyHandler object is not initialized yet.");
#endif
            _isRestoring = true;

            while (_currentEnergy < _maxEnergy)
            {
                _currentTime = CurrentTime;
                var counter = _nextEnergyTime;
                var isAdding = false;

                while (_currentTime >= counter)
                {
                    if (_currentEnergy >= _maxEnergy) break;

                    isAdding = true;
                    CurrentEnergy++;
                    var timeToAdd = _lastEnergyTime > counter ? _lastEnergyTime : counter;
                    counter = timeToAdd + _restoreDuration;
                }

                if (isAdding)
                {
                    _lastEnergyTime = _currentTime;
                    _nextEnergyTime = counter;
                    SaveValues();
                }


                if (token.IsCancellationRequested) break;
                await Task.Yield();
            }

            _isRestoring = false;
        }

        private void SaveValues() => SaveDelegate?.Invoke(new SaveData(_currentEnergy, _lastEnergyTime, _nextEnergyTime));

        private void StartRestoring()
        {
            _tokenSource = new CancellationTokenSource();
            RestoreAsync(_tokenSource.Token);
        }

        public struct SaveData
        {
            public int currentEnergy;
            public long lastEnergyTime;
            public long nextEnergyTime;

            public SaveData(int currentEnergy, long lastEnergyTime, long nextEnergyTime)
            {
                this.currentEnergy = currentEnergy;
                this.lastEnergyTime = lastEnergyTime;
                this.nextEnergyTime = nextEnergyTime;
            }
        }
    }
}
