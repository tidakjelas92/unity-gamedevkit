﻿using UnityEngine;

namespace UMNP.GamedevKit.Core
{
    [DisallowMultipleComponent]
    public abstract class Singleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        [SerializeField] private bool _dontDestroyOnLoad;

        private static T _instance;

        public static T Instance => _instance;

        protected virtual void Awake()
        {
            if (_instance && _instance != this as T)
            {
                Destroy(gameObject);
                return;
            }

            _instance = this as T;

            if (_dontDestroyOnLoad) DontDestroyOnLoad(this);
        }
    }
}
