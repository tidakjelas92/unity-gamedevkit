using System;

using UnityEngine;


namespace UMNP.GamedevKit.Core
{
    /// <summary>
    /// Wrapper to allow Unity to serialize System.Guid in editor.
    /// </summary>
    /// <remarks>
    /// Will try to return System.Guid wherever possible.
    /// </remarks>
    [Serializable]
    public struct SerializableGuid : ISerializationCallbackReceiver
    {
        public string stringGuid;
        public Guid guid;
#if UNITY_EDITOR
        public bool isValid;
#endif // UNITY_EDITOR

        public SerializableGuid(Guid guid)
        {
            this.guid = guid;
            stringGuid = guid.ToString();
#if UNITY_EDITOR
            isValid = true;
#endif // UNITY_EDITOR
        }

        public SerializableGuid(string guid)
        {
            this.guid = new Guid(guid);
            stringGuid = guid;
#if UNITY_EDITOR
            isValid = true;
#endif // UNITY_EDITOR
        }

        public static implicit operator string(SerializableGuid serializableGuid) => serializableGuid.guid.ToString();
        public static implicit operator Guid(SerializableGuid serializableGuid) => new Guid(serializableGuid.stringGuid);
        public static bool operator==(SerializableGuid serializableGuid1, SerializableGuid serializableGuid2) => serializableGuid1.guid == serializableGuid2.guid;
        public static bool operator!=(SerializableGuid serializableGuid1, SerializableGuid serializableGuid2) => serializableGuid1.guid != serializableGuid2.guid;

        public override bool Equals(object obj)
        {
            return guid.Equals((SerializableGuid)obj);
        }

        public override int GetHashCode()
        {
            return guid.GetHashCode();
        }

        public void OnBeforeSerialize()
        {
#if UNITY_EDITOR
            isValid = Guid.TryParse(stringGuid, out guid);
#endif // UNITY_EDITOR
        }

        public void OnAfterDeserialize() {}
    }
}
