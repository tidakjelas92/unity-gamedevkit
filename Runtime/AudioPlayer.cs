using UMNP.GamedevKit.Structures;
using UnityEngine;

namespace UMNP.GamedevKit
{
    public class AudioPlayer : MonoBehaviour
    {
        [SerializeField] private AudioContainer _audio;
        [SerializeField] private string _targetChannelName;

        public void Play() =>
            AudioManager.Instance.GetChannel(_targetChannelName)?.PlayOneShot(_audio);
    }
}
