﻿using System.Collections.Generic;
using UMNP.GamedevKit.Utils;
using UnityEngine;

namespace UMNP.GamedevKit.GameplayHelpers
{
    /// <summary>
    /// Content fitter for 2D orthographic camera.
    /// </summary>
    [RequireComponent(typeof(Camera))]
    public class CameraContentFitter2D : MonoBehaviour
    {
        private const float OrthographicSizeFallback = 3.0f;

        [Tooltip("Points that the camera has to fit in.")]
        [SerializeField] private List<Vector2> _points = new List<Vector2>();
        [Tooltip("Offset to add to the points after fitting. (In unity units)")]
        [SerializeField] private float _offset = 2.0f;

        private Camera _camera;
        private Transform _transform;

        public List<Vector2> Points { get => _points; set => _points = value; }
        public float Offset { get => _offset; set => _offset = value; }

        private void Awake()
        {
            _camera = GetComponent<Camera>();
            _transform = transform;
        }

        [ContextMenu("Fit")]
        public void Fit()
        {
            if (_points.Count == 0)
            {
                Debug.LogWarning("There are no points to fit into camera.", this);
                return;
            }
            else if (_points.Count == 1)
            {
                var target = new Vector3(_points[0].x, _points[0].y, _transform.position.z);
                if (_offset == 0.0f)
                    SnapToPosition(target, OrthographicSizeFallback);
                else
                    SnapToPosition(target, _offset * 2.0f);

                return;
            }

            var pointsArray = _points.ToArray();
            var topmost = Algorithms.FindTopmostPoint(pointsArray);
            var rightmost = Algorithms.FindRightmostPoint(pointsArray);
            var bottommost = Algorithms.FindBottommostPoint(pointsArray);
            var leftmost = Algorithms.FindLeftmostPoint(pointsArray);

            var aspectRatio = _camera.aspect;
            var verticalCenter = (topmost.y + bottommost.y) / 2.0f;
            var horizontalCenter = (leftmost.x + rightmost.x) / 2.0f;

            var minimumSize = 0.0f;
            var orthoSize = 0.0f;
            if (aspectRatio >= 1)
            {
                minimumSize = (rightmost.x - horizontalCenter) / aspectRatio + _offset;
                orthoSize = topmost.y - verticalCenter + _offset;
            }
            else
            {
                minimumSize = topmost.y - verticalCenter + _offset;
                orthoSize = (rightmost.x - horizontalCenter) / aspectRatio + _offset;
            }
            orthoSize = Mathf.Max(minimumSize, orthoSize);

            SnapToPosition(new Vector3(horizontalCenter, verticalCenter, _transform.position.z), orthoSize);
        }

        private void SnapToPosition(in Vector3 position, float orthographicSize)
        {
            _transform.position = position;
            _camera.orthographicSize = orthographicSize;
        }
    }
}
