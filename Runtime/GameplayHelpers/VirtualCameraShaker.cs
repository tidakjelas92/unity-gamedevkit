using Cinemachine;
using UnityEngine;

namespace UMNP.GamedevKit.GameplayHelpers
{
    [RequireComponent(typeof(CinemachineVirtualCamera))]
    public class VirtualCameraShaker : MonoBehaviour
    {
        [Header("Parameters")]
        [SerializeField] private float _amplitude;
        [SerializeField] private float _duration;
        [SerializeField] private float _frequency;
        [SerializeField] private AnimationCurve _ease;

        private CinemachineBasicMultiChannelPerlin _noise;
        private float _timeElapsed;
        private bool _isActive;

        private void Awake() =>
            _noise = GetComponent<CinemachineVirtualCamera>().GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();

        private void OnDisable() => Stop();

        private void Update()
        {
            if (!_isActive) return;

            _timeElapsed += Time.deltaTime;

            var progress = _timeElapsed / _duration;
            var ease = _ease.Evaluate(progress);
            _noise.m_AmplitudeGain = _amplitude * ease;
            _noise.m_FrequencyGain = _amplitude * ease;

            if (_timeElapsed < _duration) return;
            Stop();
        }

        [ContextMenu("Shake")]
        public void Shake() => _isActive = true;

        [ContextMenu("Stop")]
        public void Stop()
        {
            _isActive = false;
            _timeElapsed = 0.0f; 
            _noise.m_AmplitudeGain = 0.0f;
            _noise.m_FrequencyGain = 0.0f;
        }
    }
}
