using UnityEngine;
using UnityEngine.EventSystems;

namespace UMNP.GamedevKit.GameplayHelpers
{
    /// <summary>
    /// For use with touch devices.
    /// </summary>
    /// <remarks>
    /// In order for the events to work, the gameObject must have a raycast target component.
    /// An empty image component would work.<br/>
    /// The size of the rectTransform determines the area that the player can interact with.
    /// </remarks>
    public class DynamicJoystick : MonoBehaviour
    {
        [Header("Private References")]
        [SerializeField] private RectTransform _canvasRectTransform;
        [SerializeField] private RectTransform _joystickBase;
        [SerializeField] private RectTransform _stick;

        [Header("Parameters")]
        [SerializeField, Tooltip("Normalized against joystick's size")] private float _dragRadius = 0.35f;
        private Vector2 _joystickPosition = Vector2.zero;
        private Vector2 _input = Vector2.zero;

        public Vector2 Input => _input;
        protected RectTransform CanvasRectTransform => _canvasRectTransform;
        protected RectTransform JoystickBase => _joystickBase;

        protected virtual void Start() => ResetJoystick();
        protected virtual void OnDisable() => ResetJoystick();

        public void OnDrag(PointerEventData eventData)
        {
            var offset = GetPositionOnRect(_canvasRectTransform, eventData.position) - _joystickPosition;

            var clampedOffset = Vector2.ClampMagnitude(offset, _dragRadius * _joystickBase.sizeDelta.x / 2.0f);
            _stick.anchoredPosition = clampedOffset;

            _input = clampedOffset / (_joystickBase.sizeDelta / 2.0f) / _dragRadius;
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            _joystickBase.position = eventData.position;
            _joystickPosition = GetPositionOnRect(_canvasRectTransform, _joystickBase.position);
            _joystickBase.gameObject.SetActive(true);
        }

        public void OnPointerUp(PointerEventData eventData) => ResetJoystick();

        private void ResetJoystick()
        {
            _joystickBase.gameObject.SetActive(false);
            _stick.anchoredPosition = Vector2.zero;
            _input = Vector2.zero;
        }

        /// <summary>
        /// Transforms screen position into rect transform space.
        /// </summary>
        /// <remarks>
        /// With CanvasScaler mode set to 'Scale With Screen Size', the rect transform will scale according to the reference resolution.
        /// Thus, screen position will no longer be equivalent to position in rect transform.
        /// </remarks>
        public static Vector2 GetPositionOnRect(in RectTransform rectTransform, in Vector2 screenPosition) =>
            rectTransform.sizeDelta * screenPosition / new Vector2(Screen.width, Screen.height);
    }
}
