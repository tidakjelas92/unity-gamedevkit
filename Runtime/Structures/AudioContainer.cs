using System;
using UnityEngine;

namespace UMNP.GamedevKit.Structures
{
    [Serializable]
    public class AudioContainer
    {
        [SerializeField] private AudioClip _clip;
        [Range(0.0f, 1.0f)]
        [SerializeField] private float _volume = 1.0f;

        public AudioClip Clip => _clip;
        public float Volume => _volume;

        public AudioContainer() { }

        public AudioContainer(AudioClip clip, float volume = 1.0f)
        {
            this._clip = clip;
            this._volume = volume;
        }
    }
}