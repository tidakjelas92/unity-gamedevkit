using System;

namespace UMNP.GamedevKit.Structures
{
    [Serializable]
    public enum MouseButton
    {
        Left,
        Right,
        Middle,
        Forward,
        Back
    }
}
