using System;

namespace UMNP.GamedevKit.Structures
{
    [Serializable]
    public enum Axis
    {
        X,
        Y,
        Z
    }
}
