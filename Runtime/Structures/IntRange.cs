using System;
using UnityEngine;

namespace UMNP.GamedevKit.Structures
{
    [Serializable]
    public class IntRange
    {
        [SerializeField] private int _min;
        [SerializeField] private int _max;

        public int Min
        {
            get => _min;
            set => _min = Mathf.Min(value, _max);
        }
        public int Max
        {
            get => _max;
            set => _max = Mathf.Max(value, _min);
        }
        public int Delta => _max - _min;

        public IntRange() { }
        public IntRange(int min, int max)
        {
            _min = min;
            _max = max;
        }
    }
}
