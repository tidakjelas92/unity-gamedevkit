namespace UMNP.GamedevKit.Structures
{
    public struct Status
    {
        public bool success;
        public string message;

        public static Status Success => new Status(true, string.Empty);

        public Status(bool success, string message)
        {
            this.success = success;
            this.message = message;
        }
    }
}