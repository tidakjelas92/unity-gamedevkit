namespace UMNP.GamedevKit.Interfaces
{
    public interface IGamemode
    {
        bool IsActive { get; set; }
        void Initialize();
        void Update();
        void Reload();
        void Unload();
    }
}
