# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.3.1] - 2022-09-15

### Changed

- PoolObject id now defaults to -1 so new unregistered PoolObjects will be invalid.
- Removed "Runtime" from asmdef name because redundancy
- Removed SessionDataFilename from ApplicationData because unused
- WindowScreenshot to ScreenshotWindow for consistency

## Added

- Some overloads for PoolObject.Spawn
- Algorithms.GetRoundedString
- RandomPicker
- CryptographerWindow
- JsonHandler.GetDictionary
- AudioContainer
- UI.DeveloperPanel and Services.DeveloperTools
- Utils.ColorOverride, Utils.PositionOverride, Utils.TMPColorOverride
- DefaultSceneOnPlayPicker

## [0.2.0] - 2022-04-08

### Changed

- Logger to Logging: To avoid name clash with Unity
- UI.SafeArea.FitAll access modifier to public

### Added

- UI.RaycastTarget: Simple component to accept ui events without drawing anything

## [0.1.1] - 2022-04-01

### Fixed

- Safe area starting with null offset

## [0.1.0] - 2022-03-31

### Added

- Singleton and SerializableGuid
- Gameplay Helpers: CameraControl2D and CameraContentFitter2D
- Interfaces: IGamemode
- IO: Filesystem
- Pooling: Standard pooling system and particle system pooling
- Structures: Some structures and dataclasses.
- UI: SafeArea, and some boilerplate abstract classes
- Utils: Various utilities

[unreleased]: https://gitlab.com/tidakjelas92/unity-gamedevkit/-/compare/v0.2.0...main
[0.2.0]: https://gitlab.com/tidakjelas92/unity-gamedevkit/-/compare/v0.1.1...v0.2.0
[0.1.1]: https://gitlab.com/tidakjelas92/unity-gamedevkit/-/compare/v0.1.0...v0.1.1
[0.1.0]: https://gitlab.com/tidakjelas92/unity-gamedevkit/-/tags/v0.1.0
