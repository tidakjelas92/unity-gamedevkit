using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace UMNP.GamedevKit.Utils
{
    public class DefaultSceneOnPlayPicker : MonoBehaviour
    {
        [MenuItem("Tools/GamedevKit/Pick Default Scene On Play")]
        public static void Pick()
        {
            var path = EditorUtility.OpenFilePanelWithFilters("Select Scene", "", new string[] { "Scene", "unity" });

            if (path == "") return;

            path = path.Replace(Application.dataPath, "Assets");

            var scene = AssetDatabase.LoadAssetAtPath<SceneAsset>(path);
            if (!scene)
            {
                Debug.LogError("Specified scene is not found.");
                return;
            }

            EditorSceneManager.playModeStartScene = scene;
            Debug.Log($"Default scene is set to: {path}");
        }
    }
}
