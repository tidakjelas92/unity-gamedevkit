using System;
using UnityEditor;
using UnityEngine;

namespace UMNP.GamedevKit.Utils
{
    public class GuidGenerator : MonoBehaviour
    {
        [MenuItem("Tools/GamedevKit/GuidGenerator/Generate")]
        public static void Generate()
        {
            Guid newGuid = Guid.NewGuid();
            GUIUtility.systemCopyBuffer = newGuid.ToString();
            Debug.Log($"Successfully generated new GUID: {newGuid} at clipboard.");
        }
    }
}