using System;
using System.IO;

using UnityEngine;
using UnityEditor;

namespace UMNP.GamedevKit.Utils
{
    internal static class JavaPathFixer
    {
        [InitializeOnLoadMethod]
        private static void FixJavaHome()
        {
            var currentPath = Environment.GetEnvironmentVariable("JAVA_HOME");
            var newPath = Path.Combine(EditorApplication.applicationPath, "PlaybackEngines/AndroidPlayer/OpenJDK")
                .Replace("Editor/Unity/", "Editor/Data/");

            if (currentPath == newPath)
                return;

            Environment.SetEnvironmentVariable("JAVA_HOME", newPath);
            Debug.Log($"JAVA_HOME in editor was: {currentPath}\nJAVA_HOME is now set to: {Environment.GetEnvironmentVariable("JAVA_HOME")}");
        }
    }
}
