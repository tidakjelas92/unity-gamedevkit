// Source
// https://arvin.blog.csdn.net/article/details/106380122

using UnityEditor;
using UnityEngine;

namespace UMNP.GamedevKit.Utils
{
    [CustomEditor(typeof(DeletePreventor))]
    public class DeletePreventorEditor : Editor
    {
        private void OnEnable() =>
            EditorApplication.hierarchyWindowItemOnGUI += OnHierarchyGui;

        private void OnDisable() =>
            EditorApplication.hierarchyWindowItemOnGUI -= OnHierarchyGui;

        private void OnHierarchyGui(int instanceID, Rect selectionRect) => InterceptKeyboardDelete();

        private void InterceptKeyboardDelete()
        {
            var e = Event.current;
            if (e.keyCode != KeyCode.Delete) return;
            e.type = EventType.Used;
        }
    }
}
