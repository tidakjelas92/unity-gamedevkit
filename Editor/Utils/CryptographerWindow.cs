using System.Security.Cryptography;
using UnityEditor;
using UnityEngine;

namespace UMNP.GamedevKit.Utils
{
    public class CryptographerWindow : EditorWindow
    {
        private Cryptographer _cryptographer = new Cryptographer();
        private string _original = string.Empty;
        private string _encrypted = string.Empty;

        [MenuItem("Tools/GamedevKit/Cryptographer")]
        private static void Init()
        {
            var window = (CryptographerWindow)GetWindow(typeof(CryptographerWindow));
            window.titleContent = new GUIContent("Cryptographer");
            window.position = new Rect(0, 0, 300, 100);
            window.ShowUtility();
        }

        private void OnGUI()
        {
            _cryptographer.Mode = (CipherMode)EditorGUILayout.EnumPopup("Cipher Mode", _cryptographer.Mode);
            _cryptographer.Padding = (PaddingMode)EditorGUILayout.EnumPopup("Padding Mode", _cryptographer.Padding);
            _cryptographer.Key = EditorGUILayout.TextField("Key", _cryptographer.Key);
            _cryptographer.IV = EditorGUILayout.TextField("IV", _cryptographer.IV);

            if (GUILayout.Button("Generate Key"))
            {
                _cryptographer.GenerateKey(_cryptographer.Mode, _cryptographer.Padding);
                EditorGUIUtility.ExitGUI();
            }

            GUILayout.Space(20.0f);

            GUILayout.Label("Original text");
            _original = GUILayout.TextArea(_original);
            GUILayout.Label("Encrypted text");
            _encrypted = GUILayout.TextArea(_encrypted);

            if (GUILayout.Button("Encrypt"))
            {
                _encrypted = _cryptographer.Encrypt(_original);
                EditorGUIUtility.ExitGUI();
            }

            if (GUILayout.Button("Decrypt"))
            {
                _original = _cryptographer.Decrypt(_encrypted);
                EditorGUIUtility.ExitGUI();
            }
        }
    }
}