﻿using System;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace UMNP.GamedevKit.Utils
{
    public class ScreenshotWindow : EditorWindow
    {
        private string _screenshotDirectory = Path.Combine(Environment.GetEnvironmentVariable("HOME"), "Pictures");
        private int _superSize = 1;

        [MenuItem("Tools/GamedevKit/Screenshot")]
        private static void Init()
        {
            var window = (ScreenshotWindow)GetWindow(typeof(ScreenshotWindow));
            window.titleContent = new GUIContent("Screenshot");
            window.position = new Rect(0, 0, 300, 100);
            window.ShowUtility();
        }

        private void OnGUI()
        {
            _screenshotDirectory = EditorGUILayout.TextField("Screenshot Directory", _screenshotDirectory);
            _superSize = EditorGUILayout.IntField("Super Size", _superSize);

            var guiStyle = GUI.skin.label;
            guiStyle.wordWrap = true;
            GUILayout.Label("File might not appear immediately, this is normal. Please wait for a few seconds.", guiStyle);

            if (GUILayout.Button("Capture"))
            {
                Capture(_screenshotDirectory, _superSize);
                EditorGUIUtility.ExitGUI();
            }
        }

        private void Capture(in string directory, int superSize)
        {
            var timestamp = DateTime.Now;
            var stampString = string.Format("Unity_{0}-{1:00}-{2:00}_{3:00}-{4:00}-{5:00}", timestamp.Year, timestamp.Month, timestamp.Day, timestamp.Hour, timestamp.Minute, timestamp.Second);
            var fullPath = Path.Combine(directory, stampString + ".png");
            ScreenCapture.CaptureScreenshot(fullPath, superSize);

            Debug.Log($"Taking Screenshot: {fullPath}");
        }
    }
}