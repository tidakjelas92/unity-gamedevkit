using UnityEditor;
using UnityEngine;

namespace UMNP.GamedevKit.Structures
{
    [CustomPropertyDrawer(typeof(IntRange))]
    public class IntRangeEditor : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var min = property.FindPropertyRelative("_min");
            var max = property.FindPropertyRelative("_max");

            EditorGUI.BeginProperty(position, label, property);
            {
                var rect = position;
                rect.width = position.width * 0.3f;
                EditorGUI.LabelField(rect, label);

                var contentWidth = position.width - rect.width;
                var halfContentWidth = contentWidth * 0.5f;
                rect.x += rect.width;
                rect.width = 30.0f;
                EditorGUI.LabelField(rect, "Min:");

                rect.x += rect.width;
                rect.width = halfContentWidth - rect.width;
                min.intValue = Mathf.Min(EditorGUI.IntField(rect, min.intValue), max.intValue);

                rect.x += rect.width;
                rect.width = 30.0f;
                EditorGUI.LabelField(rect, "Max:");

                rect.x += rect.width;
                rect.width = halfContentWidth - rect.width;
                max.intValue = Mathf.Max(EditorGUI.IntField(rect, max.intValue), min.intValue);
            }
            EditorGUI.EndProperty();
        }
    }
}
