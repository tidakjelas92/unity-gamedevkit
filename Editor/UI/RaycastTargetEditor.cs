using UnityEditor;
using UnityEditor.UI;
using UnityEngine;

namespace UMNP.GamedevKit.UI
{
    [CanEditMultipleObjects, CustomEditor(typeof(RaycastTarget), false)]
    public class RaycastTargetEditor : GraphicEditor
    {
        public override void OnInspectorGUI()
        {
            base.serializedObject.Update();
            EditorGUI.BeginDisabledGroup(true);
            EditorGUILayout.PropertyField(base.m_Script, new GUILayoutOption[0]);
            EditorGUI.EndDisabledGroup();
            // skipping AppearanceControlsGUI
            base.RaycastControlsGUI();
            base.serializedObject.ApplyModifiedProperties();
        }
    }
}