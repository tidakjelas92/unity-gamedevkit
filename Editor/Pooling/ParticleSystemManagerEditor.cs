using UnityEditor;
using UnityEngine;

namespace UMNP.GamedevKit.Pooling
{
    [CustomEditor(typeof(ParticleSystemManager))]
    public class ParticleSystemManagerEditor : Editor
    {
        private ParticleSystemManager _particleSystemManager;

        public override void OnInspectorGUI()
        {
            _particleSystemManager = target as ParticleSystemManager;

            base.OnInspectorGUI();

            if (GUILayout.Button("Update ID"))
            {
                _particleSystemManager.UpdateID();
                GUIUtility.ExitGUI();
            }
        }
    }
}
