using NUnit.Framework;
using UMNP.GamedevKit.Utils;
using UnityEngine;

namespace Utils
{
    public class AlgorithmsTests
    {
        [Test]
        public void Vector2IntClamp_NormalCondition()
        {
            var value = new Vector2Int(4, 1);
            var min = Vector2Int.zero;
            var max = new Vector2Int(10, 10);

            value = Algorithms.Clamp(value, min, max);

            var expectedValue = new Vector2Int(4, 1);
            Assert.AreEqual(expectedValue, value);
        }

        [Test]
        public void Vector2IntClamp_ValueAboveMax()
        {
            var value = new Vector2Int(15, 20);
            var min = Vector2Int.zero;
            var max = new Vector2Int(10, 10);

            value = Algorithms.Clamp(value, min, max);

            var expectedValue = new Vector2Int(10, 10);
            Assert.AreEqual(expectedValue, value);
        }

        [Test]
        public void Vector2IntClamp_ValueBelowMin()
        {
            var value = new Vector2Int(-5, -3);
            var min = Vector2Int.zero;
            var max = new Vector2Int(10, 10);

            value = Algorithms.Clamp(value, min, max);

            var expectedValue = Vector2Int.zero;
            Assert.AreEqual(expectedValue, value);
        }

        [Test]
        public void Vector2IntClamp_BadParameters()
        {
            var value = new Vector2Int(-5, -3);
            var min = new Vector2Int(10, 10);
            var max = Vector2Int.zero;

            value = Algorithms.Clamp(value, min, max);

            var expectedValue = new Vector2Int(10, 10);
            Assert.AreEqual(expectedValue, value);
        }

        [Test]
        public void Vector2IntClamp_MaxValue()
        {
            var value = new Vector2Int(-3, 5);
            var min = Vector2Int.zero;
            var max = new Vector2Int(int.MaxValue, int.MaxValue);

            value = Algorithms.Clamp(value, min, max);

            Vector2Int expectedValue = new Vector2Int(0, 5);
            Assert.AreEqual(expectedValue, value);
        }
    }
}
