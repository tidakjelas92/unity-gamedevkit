using NUnit.Framework;
using UMNP.GamedevKit.Structures;
using UMNP.GamedevKit.Utils;
using UnityEngine;


namespace IO
{
    public class NameValidatorTests
    {
        private string[] _illegalNamesWindows = new string[] {
            "CON", "con", "cOn",
            "PRN", "prn", "pRn",
            "AUX", "aux", "AuX",
            "COM0", "com0", "cOM0",
            "COM1", "com1", "CoM1",
            "COM2", "com2", "cOM2",
            "COM3", "com3", "CoM3",
            "COM4", "com4", "cOM4",
            "COM5", "com5", "CoM5",
            "COM6", "com6", "cOM6",
            "COM7", "com7", "CoM7",
            "COM8", "com8", "cOM8",
            "COM9", "com9", "CoM9",
            "LPT0", "lpt0", "lPT0",
            "LPT1", "lpt1", "LpT1",
            "LPT2", "lpt2", "lPT2",
            "LPT3", "lpt3", "LpT3",
            "LPT4", "lpt4", "lPT4",
            "LPT5", "lpt5", "LpT5",
            "LPT6", "lpt6", "lPT6",
            "LPT7", "lpt7", "LpT7",
            "LPT8", "lpt8", "lPT8",
            "LPT9", "lpt9", "LpT9",
            ".", ".."
        };

        private string[] _illegalCharacterNames = new string[] {
            "\\ejfiejcneu",
            "iam/acceptable",
            "PerfectlyNormal:",
            "\"QuotationMark\"",
            "this|that",
            "1 < 2",
            "2 > 1",
            "you are a *",
            "wtf?"
        };

        [Test]
        public void NameValidatorCheckFolderNameWindows_NormalCondition()
        {
            var name = "A very normal name123";
            var status = NameValidator.CheckFolderNameWindows(name);

            Assert.That<bool>(status.success, Is.True);
        }

        [Test]
        public void NameValidatorCheckFolderNameWindows_EmptyString()
        {
            var name = string.Empty;
            var status = NameValidator.CheckFolderNameWindows(name);

            Assert.That<bool>(status.success, Is.False);
        }

        [Test]
        public void NameValidatorCheckFolderNameWindows_IllegalName1()
        {
            var name = "CON";
            var status = NameValidator.CheckFolderNameWindows(name);

            Assert.That<bool>(status.success, Is.False);
        }

        [Test]
        public void NameValidatorCheckFolderNameWindows_IllegalNamesWithDifferentCasing()
        {
            var result = false;

            for (var i = 0; i < _illegalNamesWindows.Length; i++)
            {
                var status = NameValidator.CheckFolderNameWindows(_illegalNamesWindows[i]);
                result = result || status.success;
            }

            Assert.That(result, Is.False);
        }

        [Test]
        public void NameValidatorCheckFolderNameWindows_IllegalCharacters()
        {
            var names = new string[] {
                "\\ejfiejcneu",
                "iam/acceptable",
                "PerfectlyNormal:",
                "\"QuotationMark\"",
                "this|that",
                "1 < 2",
                "2 > 1",
                "you are a *",
                "wtf?"
            };
            var result = false;

            for (var i = 0; i < names.Length; i++)
            {
                var status = NameValidator.CheckFolderNameWindows(names[i]);
                result = result || status.success;
            }

            Assert.That(result, Is.False);
        }

        [Test]
        public void NameValidatorCheckFolderNameWindows_TrailingDot()
        {
            var name = "this_name_endswith_dot.";
            var status = NameValidator.CheckFolderNameWindows(name);

            Assert.That<bool>(status.success, Is.False);
        }

        [Test]
        public void NameValidatorCheckFolderNameWindows_TrailingWhitespace()
        {
            var name = "this_name_endswith_whitespace       ";
            var status = NameValidator.CheckFolderNameWindows(name);

            Assert.That<bool>(status.success, Is.False);
        }

        [Test]
        public void NameValidatorCheckFileNameWindows_NormalCondition()
        {
            var name = "test";
            var ext = "json";
            var status = NameValidator.CheckFileNameWindows(name, ext);

            Assert.That<bool>(status.success, Is.True);
        }

        [Test]
        public void NameValidatorCheckFileNameWindows_NormalConditionNoExtension()
        {
            var name = "normal";
            var status = NameValidator.CheckFileNameWindows(name);

            Assert.That<bool>(status.success, Is.True);
        }

        [Test]
        public void NameValidatorCheckFileNameWindows_IllegalNamesWithDifferentCasing()
        {
            var result = false;

            foreach (var illegalName in _illegalNamesWindows)
            {
                var status = NameValidator.CheckFileNameWindows(illegalName);
                result = result || status.success;
            }

            Assert.That(result, Is.False);
        }

        [Test]
        public void NameValidatorCheckFileNameWindows_IllegalCharacters()
        {
            var result = false;

            foreach (var illegalName in _illegalCharacterNames)
            {
                var status = NameValidator.CheckFileNameWindows(illegalName);
                result = result || status.success;
            }

            Assert.That(result, Is.False);
        }

        [Test]
        public void NameValidatorCheckFileNameWindows_ExtensionsWithIllegalCharacters()
        {
            var result = false;

            foreach (var illegalName in _illegalCharacterNames)
            {
                var status = NameValidator.CheckFileNameWindows("PerfectlyNormalName", illegalName);
                result = result || status.success;
            }

            Assert.That(result, Is.False);
        }
    }
}
