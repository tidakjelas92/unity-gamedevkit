using System.Collections;
using NUnit.Framework;
using UMNP.GamedevKit.GameplayHelpers;
using UnityEngine;
using UnityEngine.TestTools;

namespace Utils
{
    public class CameraContentFitter2DTests
    {
        [UnityTest]
        public IEnumerator FitToCamera_NormalCondition_Landscape()
        {
            // Arrange
            Screen.SetResolution(960, 540, false);
            yield return null;
            var cameraObject = new GameObject();
            var camera = cameraObject.AddComponent<Camera>();
            var cameraContentFitter = cameraObject.AddComponent<CameraContentFitter2D>();
            var cameraTransform = camera.transform;

            cameraTransform.position = new Vector3(0.0f, 0.0f, -10.0f);
            camera.orthographic = true;
            cameraContentFitter.Offset = 2.0f;
            cameraContentFitter.Points.Add(new Vector2(-3.39f, 1.19f));
            cameraContentFitter.Points.Add(new Vector2(-0.8f, 2.3f));
            cameraContentFitter.Points.Add(new Vector2(3.5f, 2.0f));

            // Act
            cameraContentFitter.Fit();
            yield return null;

            // Assert
            const float ExpectedValue = 3.94f;
            Assert.AreEqual(ExpectedValue, camera.orthographicSize, 0.01f);
        }

        [UnityTest]
        public IEnumerator FitToCamera_SinglePoint_Landscape()
        {
            // Arrange
            Screen.SetResolution(960, 540, false);
            yield return null;
            var cameraObject = new GameObject();
            var camera = cameraObject.AddComponent<Camera>();
            var cameraContentFitter = cameraObject.AddComponent<CameraContentFitter2D>();
            var cameraTransform = camera.transform;

            cameraTransform.position = new Vector3(0.0f, 0.0f, -10.0f);
            camera.orthographic = true;
            cameraContentFitter.Offset = 2.0f;
            cameraContentFitter.Points.Add(new Vector2(-3.39f, 1.19f));

            // Act
            cameraContentFitter.Fit();
            yield return null;

            // Assert
            var expectedValue = cameraContentFitter.Offset * 2;
            Assert.AreEqual(expectedValue, camera.orthographicSize, 0.01f);
        }

        [UnityTest]
        public IEnumerator FitToCamera_ZeroPoint_Landscape()
        {
            // Arrange
            Screen.SetResolution(960, 540, false);
            yield return null;
            var cameraObject = new GameObject();
            var camera = cameraObject.AddComponent<Camera>();
            var cameraContentFitter = cameraObject.AddComponent<CameraContentFitter2D>();
            var cameraTransform = camera.transform;

            var currentPosition = new Vector3(0.0f, 0.0f, -10.0f);
            cameraTransform.position = currentPosition;
            camera.orthographic = true;
            var currentOrthographicSize = camera.orthographicSize;
            cameraContentFitter.Offset = 2.0f;

            // Act
            cameraContentFitter.Fit();
            yield return null;

            // Assert
            Assert.AreEqual(currentOrthographicSize, camera.orthographicSize, 0.01f);
            Assert.AreEqual(currentPosition, cameraTransform.position);
        }

        [UnityTest]
        public IEnumerator FitToCamera_NormalCondition_Portrait()
        {
            // Arrange
            Screen.SetResolution(540, 960, false);
            yield return null;
            var cameraObject = new GameObject();
            var camera = cameraObject.AddComponent<Camera>();
            var cameraContentFitter = cameraObject.AddComponent<CameraContentFitter2D>();
            var cameraTransform = camera.transform;

            cameraTransform.position = new Vector3(0.0f, 0.0f, -10.0f);
            camera.orthographic = true;
            cameraContentFitter.Offset = 2.0f;
            cameraContentFitter.Points.Add(new Vector2(-3.39f, 1.19f));
            cameraContentFitter.Points.Add(new Vector2(-0.8f, 2.3f));
            cameraContentFitter.Points.Add(new Vector2(3.5f, 2.0f));

            // Act
            cameraContentFitter.Fit();
            yield return null;

            // Assert
            const float ExpectedValue = 8.12f;
            Assert.AreEqual(ExpectedValue, camera.orthographicSize, 0.01f);
        }

        [UnityTest]
        public IEnumerator FitToCamera_ZeroPoint_Portrait()
        {
            // Arrange
            Screen.SetResolution(540, 960, false);
            yield return null;
            var cameraObject = new GameObject();
            var camera = cameraObject.AddComponent<Camera>();
            var cameraContentFitter = cameraObject.AddComponent<CameraContentFitter2D>();
            var cameraTransform = camera.transform;

            var currentPosition = new Vector3(0.0f, 0.0f, -10.0f);
            cameraTransform.position = currentPosition;
            camera.orthographic = true;
            var currentOrthographicSize = camera.orthographicSize;
            cameraContentFitter.Offset = 2.0f;

            // Act
            cameraContentFitter.Fit();
            yield return null;

            // Assert
            Assert.AreEqual(currentOrthographicSize, camera.orthographicSize, 0.01f);
            Assert.AreEqual(currentPosition, cameraTransform.position);
        }

        [UnityTest]
        public IEnumerator FitToCamera_SinglePoint_Portrait()
        {
            // Arrange
            Screen.SetResolution(540, 960, false);
            yield return null;
            var cameraObject = new GameObject();
            var camera = cameraObject.AddComponent<Camera>();
            var cameraContentFitter = cameraObject.AddComponent<CameraContentFitter2D>();
            var cameraTransform = camera.transform;

            cameraTransform.position = new Vector3(0.0f, 0.0f, -10.0f);
            camera.orthographic = true;
            cameraContentFitter.Offset = 2.0f;
            cameraContentFitter.Points.Add(new Vector2(-3.39f, 1.19f));

            // Act
            cameraContentFitter.Fit();
            yield return null;

            // Assert
            var expectedValue = cameraContentFitter.Offset * 2;
            Assert.AreEqual(expectedValue, camera.orthographicSize, 0.01f);
        }
    }
}
